plugins {
    id("com.vigil.trading.common-dependencies") version "1.0.0"
}

version = "1.0.5"

dependencies {
    implementation("com.vigil.trading:integration:2.2.15")

    implementation("org.springframework.kafka:spring-kafka")
}
