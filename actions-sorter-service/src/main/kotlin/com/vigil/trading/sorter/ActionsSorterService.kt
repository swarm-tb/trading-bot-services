package com.vigil.trading.sorter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["com.vigil.trading"])
open class ActionsSorterService

fun main(args: Array<String>) {
    runApplication<ActionsSorterService>(*args)
}
