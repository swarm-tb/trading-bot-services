package com.vigil.trading.sorter.utility

fun <T> MutableSet<T>.clearAndCount(): Int {
    val count = this.size
    this.clear()
    return count
}
