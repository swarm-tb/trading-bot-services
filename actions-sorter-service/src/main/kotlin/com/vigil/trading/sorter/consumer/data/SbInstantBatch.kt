package com.vigil.trading.sorter.consumer.data

import com.vigil.trading.integration.records.SbTradingActionInfo
import java.util.concurrent.atomic.AtomicInteger

data class SbInstantBatch(
    val remainingSize: AtomicInteger,
    val values: MutableSet<SbTradingActionInfo> = mutableSetOf()
)
