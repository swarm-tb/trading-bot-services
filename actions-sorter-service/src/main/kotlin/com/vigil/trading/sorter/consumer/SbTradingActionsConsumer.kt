package com.vigil.trading.sorter.consumer

import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.constant.TradingAction
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.records.simulation.SbInstantContext
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.sorter.consumer.data.SbInstantBatch
import com.vigil.trading.sorter.utility.clearAndCount
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.concurrent.atomic.AtomicInteger

@Service
class SbTradingActionsConsumer(
    private val tradingActionTemplate: KafkaTemplate<String, SbTradingActionInfo>,
    private val accountUpdateTemplate: KafkaTemplate<String, Instant>,
    private val commonProperties: CommonProperties
) {
    private val log by logger()
    private val instantBatchMap = linkedMapOf<Instant, SbInstantBatch>()

    @KafkaListener(
        topics = ["#{commonProperties.getKafka().getActionsSorterTopic()}"],
        groupId = "#{commonProperties.getKafka().getActionsSorterGroupId()}"
    )
    fun consumeTradingAction(tradingActionInfo: SbTradingActionInfo) {
        //TODO fix the issue with missing account updates
        val timestamp = tradingActionInfo.timestamp
        instantBatchMap[timestamp]?.let {
            processBatch(it, tradingActionInfo)
        } ?: log.error("Instant batch is not found for timestamp: {}", timestamp)
    }

    private fun processBatch(
        instantBatch: SbInstantBatch,
        tradingActionInfo: SbTradingActionInfo
    ) {
        synchronized(this) {
            instantBatch.values.add(tradingActionInfo)
            val firstEntry = instantBatchMap.entries.first()
            processFirstEntry(firstEntry)
            removeFirstBatchIfReady(firstEntry)
        }
    }

    private fun processFirstEntry(firstEntry: MutableMap.MutableEntry<Instant, SbInstantBatch>) {
        val firstBatch = firstEntry.value
        firstBatch.values.forEach {
            if (it.tradingAction != TradingAction.NEUTRAL) {
                tradingActionTemplate.send(commonProperties.kafka.tradingActionsTopic, it)
            }
        }
    }

    private fun removeFirstBatchIfReady(firstEntry: MutableMap.MutableEntry<Instant, SbInstantBatch>) {
        val firstBatch = firstEntry.value
        val clearedCount = firstBatch.values.clearAndCount()
        if (firstBatch.remainingSize.addAndGet(-clearedCount) == 0) {
            removeBatchAndNotifyAccount(firstEntry)
        }
    }

    private fun removeBatchAndNotifyAccount(firstEntry: MutableMap.MutableEntry<Instant, SbInstantBatch>) {
        instantBatchMap.remove(firstEntry.key)
        val first = instantBatchMap.entries.firstOrNull()
        first?.let {
            accountUpdateTemplate.send(commonProperties.kafka.accountUpdateTopic, first.key)
        }
    }

    @KafkaListener(
        topics = ["#{commonProperties.getKafka().getInstantContextTopic()}"],
        groupId = "#{commonProperties.getKafka().getActionsSorterGroupId()}"
    )
    fun consumeInstantContext(instantContext: SbInstantContext) {
        synchronized(this) {
            instantBatchMap[instantContext.timestamp()] =
                SbInstantBatch(AtomicInteger(instantContext.symbolsContext().size))
        }
    }
}
