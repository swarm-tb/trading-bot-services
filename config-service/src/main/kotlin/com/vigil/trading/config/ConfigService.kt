package com.vigil.trading.config

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.config.server.EnableConfigServer

@EnableConfigServer
@SpringBootApplication(scanBasePackages = ["com.vigil.trading"])
class ConfigService

fun main(args: Array<String>) {
    runApplication<ConfigService>(*args)
}
