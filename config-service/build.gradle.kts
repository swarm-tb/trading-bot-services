plugins {
    val kotlinVersion = "1.8.22"
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
}

version = "1.0.0"

dependencies {
    implementation("com.vigil.trading:sb-common:2.2.0")

    implementation("org.springframework.cloud:spring-cloud-config-server")
}
