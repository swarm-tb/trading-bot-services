plugins {
    id("com.vigil.trading.common-dependencies") version "1.0.1"
}

version = "1.0.2"

dependencies {
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-server")
}
