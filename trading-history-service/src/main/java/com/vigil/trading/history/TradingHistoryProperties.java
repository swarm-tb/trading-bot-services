package com.vigil.trading.history;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "history")
public class TradingHistoryProperties {
  //TODO: move to config service
  private String simulationId;
}
