package com.vigil.trading.history;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.vigil.trading")
public class TradingHistoryServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(TradingHistoryServiceApplication.class, args);
  }
}
