package com.vigil.trading.history.container

import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.dto.history.SbPositionHistoryRecord
import com.vigil.trading.integration.records.SbOrder
import org.apache.commons.collections4.CollectionUtils
import java.math.BigDecimal
import java.util.*

class SymbolPositionsContainer {
    val symbolPositions = mutableMapOf<String, Deque<SbPositionHistoryRecord>>()
    var totalCommission: BigDecimal = BigDecimal.ZERO
    var totalProfit: BigDecimal = BigDecimal.ZERO

    fun addPosition(order: SbOrder): SbPositionHistoryRecord {
        val computeFunction: (BigDecimal, BigDecimal) -> BigDecimal
        val initialSize: BigDecimal
        val initialCost: BigDecimal
        if (OrderSide.BUY == order.side) {
            computeFunction = BigDecimal::add
            initialSize = order.size
            initialCost = computeFunction(order.cost, order.commission)
        } else {
            computeFunction = BigDecimal::subtract
            initialSize = -order.size
            initialCost = computeFunction(order.commission, order.cost)
        }
        val positionSummary = symbolPositions[order.symbol]
        val positions = positionSummary ?: initPositions(order)
        totalCommission = totalCommission.add(order.commission)
        if (CollectionUtils.isEmpty(positions)) {
            val newPosition = getPosition(order, initialSize, initialCost, BigDecimal.ZERO, BigDecimal.ZERO)
            positions.add(newPosition)
            return newPosition
        }
        val previousPosition = positions.last
        val newPosition = accumulatePosition(previousPosition, order, computeFunction)
        positions.add(newPosition)
        return newPosition
    }

    private fun initPositions(order: SbOrder): Deque<SbPositionHistoryRecord> {
        val positions = LinkedList<SbPositionHistoryRecord>()
        symbolPositions[order.symbol] = positions
        return positions
    }

    private fun accumulatePosition(
        previousPosition: SbPositionHistoryRecord,
        order: SbOrder,
        computeFunction: (BigDecimal, BigDecimal) -> BigDecimal
    ): SbPositionHistoryRecord {
        val newSize = computeFunction(previousPosition.totalSize, order.size)
        val cumulativePositionCost =
            computeFunction(previousPosition.totalValueUsed, computeFunction(order.cost, order.commission))
        val currentPositionCost = newSize.multiply(order.price)
        val newPnL = currentPositionCost.subtract(cumulativePositionCost)
        val pNlDelta = newPnL.subtract(previousPosition.pNl)
        totalProfit = totalProfit.add(pNlDelta)
        return getPosition(order, newSize, cumulativePositionCost, newPnL, pNlDelta)
    }

    private fun getPosition(
        order: SbOrder,
        totalSize: BigDecimal,
        totalCost: BigDecimal,
        pNl: BigDecimal,
        pNlDelta: BigDecimal
    ) = SbPositionHistoryRecord(
        order.timestamp,
        order.side,
        order.size,
        order.price,
        totalSize,
        totalCost,
        pNl,
        pNlDelta
    )
}
