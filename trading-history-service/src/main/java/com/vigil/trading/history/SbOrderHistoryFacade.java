package com.vigil.trading.history;

import com.vigil.trading.history.impl.OrderHistoryServiceBinance;
import com.vigil.trading.history.impl.OrderHistoryServiceSimulation;
import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.records.SbOrder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Component
public class SbOrderHistoryFacade {

  private final Map<SbAccount, OrderHistoryService> orderHistoryServiceMap;

  public SbOrderHistoryFacade(
      final OrderHistoryServiceBinance orderHistoryServiceBinance,
      final OrderHistoryServiceSimulation orderHistoryServiceSimulation
  ) {
    orderHistoryServiceMap = Map.of(
        SbAccount.BINANCE_USD_FUTURES, orderHistoryServiceBinance,
        SbAccount.SIMULATION, orderHistoryServiceSimulation
    );
  }

  public List<SbOrder> getOrders(final SbAccount account, final Instant fromTimestamp, final Instant toTimestamp) {
    return orderHistoryServiceMap.get(account).getOrders(fromTimestamp, toTimestamp);
  }
}
