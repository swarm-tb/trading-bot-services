package com.vigil.trading.history;

import com.vigil.trading.integration.constant.Endpoint;
import com.vigil.trading.integration.constant.Endpoint.PathVariableName;
import com.vigil.trading.integration.constant.Endpoint.RequestParamName;
import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.dto.history.TradingHistory;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@Slf4j
@RestController
@RequestMapping(Endpoint.TRADING_HISTORY)
public class TradingHistoryController {

  private final TradingHistoryService tradingHistoryService;
  private final SbOrderHistoryFacade orderHistoryFacade;

  public TradingHistoryController(
      final TradingHistoryService tradingHistoryService,
      final SbOrderHistoryFacade orderHistoryFacade
  ) {
    this.tradingHistoryService = tradingHistoryService;
    this.orderHistoryFacade = orderHistoryFacade;
  }

  @GetMapping(Endpoint.Path.HISTORY)
  public TradingHistory getTradingHistory(
      @PathVariable(PathVariableName.ACCOUNT) final SbAccount account,
      @RequestParam(RequestParamName.FROM_TIMESTAMP) final Instant fromTimestamp,
      @RequestParam(name = RequestParamName.TO_TIMESTAMP, required = false) final Instant toTimestamp
  ) {
    log.info("Fetching trading history started");
    val orders = orderHistoryFacade.getOrders(account, fromTimestamp, toTimestamp);
    val tradingHistory = tradingHistoryService.getTradingHistory(orders, account);
    log.info("Fetching trading history finished");
    return tradingHistory;
  }
}
