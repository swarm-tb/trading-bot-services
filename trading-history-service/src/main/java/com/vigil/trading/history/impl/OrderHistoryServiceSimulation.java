package com.vigil.trading.history.impl;

import com.vigil.trading.history.OrderHistoryService;
import com.vigil.trading.history.TradingHistoryProperties;
import com.vigil.trading.history.mongo.SbOrdersRepository;
import com.vigil.trading.integration.models.order.SbOrderModel;
import com.vigil.trading.integration.records.SbOrder;
import one.util.streamex.StreamEx;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

import static com.vigil.trading.integration.models.order.Constant.TIMESTAMP_FIELD;

@Service
public class OrderHistoryServiceSimulation implements OrderHistoryService {

  private final SbOrdersRepository ordersRepository;
  private final TradingHistoryProperties tradingHistoryProperties;

  public OrderHistoryServiceSimulation(
      final SbOrdersRepository ordersRepository,
      final TradingHistoryProperties tradingHistoryProperties
  ) {
    this.ordersRepository = ordersRepository;
    this.tradingHistoryProperties = tradingHistoryProperties;
  }

  @Override
  public List<SbOrder> getOrders(final Instant fromTimestamp, final Instant toTimestamp) {
    return StreamEx.of(ordersRepository.findByMetaFieldAndTimestampBetween(
                       tradingHistoryProperties.getSimulationId(),
                       fromTimestamp,
                       toTimestamp,
                       Sort.by(Direction.ASC, TIMESTAMP_FIELD)
                   ))
                   .map(OrderHistoryServiceSimulation::toTbOrder)
                   .toList();
  }

  private static SbOrder toTbOrder(final SbOrderModel orderModel) {
    return new SbOrder(
        orderModel.symbol(),
        orderModel.timestamp(),
        orderModel.status(),
        orderModel.side(),
        orderModel.type(),
        orderModel.price(),
        orderModel.size(),
        orderModel.cost(),
        orderModel.commission()
    );
  }
}
