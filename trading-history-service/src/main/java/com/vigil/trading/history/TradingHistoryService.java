package com.vigil.trading.history;

import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.dto.history.TradingHistory;
import com.vigil.trading.integration.records.SbOrder;

import java.util.List;

public interface TradingHistoryService
{

  TradingHistory getTradingHistory(final List<SbOrder> orders, final SbAccount account);
}
