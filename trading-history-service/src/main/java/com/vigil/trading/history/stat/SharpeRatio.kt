package com.vigil.trading.history.stat

import com.vigil.trading.integration.dto.history.SbPositionHistoryRecord
import com.vigil.trading.integration.utility.isZero
import org.apache.commons.math3.stat.descriptive.moment.Mean
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation
import java.lang.Double.isInfinite
import java.lang.Double.isNaN
import java.math.BigDecimal
import kotlin.math.sqrt

class SharpeRatio {
    private val standardDeviation = StandardDeviation()
    private val mean = Mean()

    fun getResult(): BigDecimal {
        // TODO: Replace commons math with custom BigDecimal implementation
        val value = sqrt(standardDeviation.n.toDouble()) * mean.result / standardDeviation.result
        return if (isNaN(value) || isInfinite(value)) BigDecimal.ZERO else value.toBigDecimal()
    }

    fun increment(tbPosition: SbPositionHistoryRecord) {
        val pNlDelta: BigDecimal = tbPosition.pNlDelta
        if (!pNlDelta.isZero()) {
            standardDeviation.increment(pNlDelta.toDouble())
            mean.increment(pNlDelta.toDouble())
        }
    }
}
