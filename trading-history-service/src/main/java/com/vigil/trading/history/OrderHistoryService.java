package com.vigil.trading.history;

import com.vigil.trading.integration.records.SbOrder;

import java.time.Instant;
import java.util.List;

public interface OrderHistoryService {
  /**
   * Fetches order history in chronological order.
   *
   * @param fromTimestamp lower bound
   * @param toTimestamp upper bound
   * @return the list of orders
   */
  List<SbOrder> getOrders(Instant fromTimestamp, Instant toTimestamp);
}
