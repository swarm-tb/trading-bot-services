package com.vigil.trading.history.container

import com.vigil.trading.integration.dto.history.SbPositionHistoryRecord
import com.vigil.trading.integration.records.TbProfit
import com.vigil.trading.integration.utility.isZero
import java.math.BigDecimal
import java.util.*

class ProfitHistoryContainer {
    val profitHistory: Deque<TbProfit> = LinkedList()
    var maxDrawdown: BigDecimal = BigDecimal.ZERO

    private var maxPeak = BigDecimal.ZERO

    fun add(position: SbPositionHistoryRecord) {
        val pNlDelta = position.pNlDelta
        val timestamp = position.timestamp
        if (!pNlDelta.isZero()) {
            try {
                val cumulativeValue = profitHistory.last.value().add(pNlDelta)
                profitHistory.add(TbProfit(timestamp, cumulativeValue))
                resolveMaxDrawdown(cumulativeValue)
            } catch (e: NoSuchElementException) {
                profitHistory.add(TbProfit(timestamp, pNlDelta))
                resolveMaxDrawdown(pNlDelta)
            }
        }
    }

    private fun resolveMaxDrawdown(cumulativeProfit: BigDecimal) {
        if (cumulativeProfit > maxPeak) {
            maxPeak = cumulativeProfit
        }
        val drawdown = maxPeak - cumulativeProfit
        if (drawdown > maxDrawdown) {
            maxDrawdown = drawdown
        }
    }
}
