package com.vigil.trading.history.container;


import com.vigil.trading.history.stat.SharpeRatio;

public record TradingHistoryContainer(
    SharpeRatio sharpeRatio,
    ProfitHistoryContainer profitHistoryContainer,
    SymbolPositionsContainer symbolPositionsContainer) {
  public static TradingHistoryContainer init() {
    return new TradingHistoryContainer(
        new SharpeRatio(),
        new ProfitHistoryContainer(),
        new SymbolPositionsContainer());
  }
}
