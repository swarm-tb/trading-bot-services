package com.vigil.trading.history.impl

import com.vigil.trading.history.TradingHistoryService
import com.vigil.trading.history.container.TradingHistoryContainer
import com.vigil.trading.integration.clients.SymbolClient
import com.vigil.trading.integration.clients.TransactionClient
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.order.OrderStatus
import com.vigil.trading.integration.dto.history.TradingHistory
import com.vigil.trading.integration.records.SbOrder
import com.vigil.trading.integration.utility.isZero
import one.util.streamex.StreamEx
import org.springframework.stereotype.Service
import java.util.stream.Collector

@Service
class TradingHistoryServiceImpl(
    private val symbolClient: SymbolClient,
    private val transactionClient: TransactionClient
) : TradingHistoryService {
    override fun getTradingHistory(orders: List<SbOrder>, account: SbAccount): TradingHistory {
        val symbols = symbolClient.getSymbols(account)
        val applicableOrders = StreamEx.of(orders)
            .parallel()
            .filter { order -> symbols.contains(order.symbol) }
            .filter { order -> isValid(order) }
            .toList()
        val tradingHistoryContainer = StreamEx.of(applicableOrders).collect(historyCollector())
        return TradingHistory(
            tradingHistoryContainer.symbolPositionsContainer.symbolPositions,
            tradingHistoryContainer.symbolPositionsContainer.totalProfit,
            tradingHistoryContainer.symbolPositionsContainer.totalCommission,
            tradingHistoryContainer.sharpeRatio.getResult(),
            tradingHistoryContainer.profitHistoryContainer.maxDrawdown,
            tradingHistoryContainer.profitHistoryContainer.profitHistory,
            transactionClient.accountBalanceHistory,
            orders.size
        )
    }

    private fun isValid(tbOrder: SbOrder): Boolean =
        OrderStatus.CLOSED == tbOrder.status && !tbOrder.size.isZero()

    private fun historyCollector(): Collector<SbOrder, TradingHistoryContainer, TradingHistoryContainer> =
        Collector.of(
            { TradingHistoryContainer.init() },
            { summary, order -> increment(summary, order) },
            { _, _ -> mergeContainers() })

    private fun increment(summary: TradingHistoryContainer, order: SbOrder) {
        val tbPosition = summary.symbolPositionsContainer.addPosition(order)
        summary.profitHistoryContainer.add(tbPosition)
        summary.sharpeRatio.increment(tbPosition)
    }

    private fun mergeContainers(): TradingHistoryContainer = throw UnsupportedOperationException()
}
