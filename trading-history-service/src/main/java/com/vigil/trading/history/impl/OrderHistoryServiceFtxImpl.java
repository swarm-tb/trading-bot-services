//package com.vigil.trading.history.impl.ftx;
//
//import com.vigil.trading.integration.constant.order.OrderSide;
//import com.vigil.trading.integration.constant.order.OrderStatus;
//import com.vigil.trading.integration.constant.order.OrderType;
//import com.vigil.trading.integration.records.TbOrder;
//import com.vigil.trading.integration.utility.TimeUtils;
//import com.vigil.trading.core.balancing.OrderHistoryService;
//import com.vigil.trading.core.utility.TradingPairUtils;
//import io.contek.invoker.ftx.api.common._Order;
//import io.contek.invoker.ftx.api.rest.user.GetOrderHistory;
//import io.contek.invoker.ftx.api.rest.user.UserRestApi;
//import lombok.extern.slf4j.Slf4j;
//import one.util.streamex.StreamEx;
//import org.springframework.stereotype.Service;
//import org.ta4j.core.num.DecimalNum;
//import org.ta4j.core.num.Num;
//
//import java.time.Instant;
//import java.util.Collections;
//import java.util.List;
//
//@Slf4j
//@Service
//public class OrderHistoryServiceFtxImpl implements OrderHistoryService {
//
//  private final UserRestApi userRestApi;
//  private final TimeUtils timeUtils;
//  private final TradingPairUtils tradingPairUtils;
//
//  public OrderHistoryServiceFtxImpl(
//          UserRestApi userRestApi,
//          final TimeUtils timeUtils,
//          final TradingPairUtils tradingPairUtils) {
//  ) {
//    this.userRestApi = userRestApi;
//    this.timeUtils = timeUtils;
//    this.tradingPairUtils = tradingPairUtils;
//  }
//
//  @Override
//  public List<TbOrder> getOrders(final Instant fromTimestamp, final Instant toTimestamp) {
//    final long fromPrepared = fromTimestamp.getEpochSecond();
//    final long toPrepared = timeUtils.getCurrentTimeIfNull(toTimestamp).getEpochSecond();
//    final List<_Order> orders = getOrdersForTimeRange(fromPrepared, toPrepared);
//    Collections.reverse(orders);
//    return StreamEx.of(orders).map(this::getOrderRecord).toList();
//  }
//
//  protected List<_Order> getOrdersForTimeRange(final long fromTimestamp, final long toTimestamp) {
//    final GetOrderHistory.Response response =
//        userRestApi.getOrderHistory().setStartTime(fromTimestamp).setEndTime(toTimestamp)
//        .submit();
//    if (response.hasMoreData) {
//      final _Order lastOrder = response.result.get(response.result.size() - 1);
//      final long lastOrderCreationTime = Instant.parse(lastOrder.createdAt).getEpochSecond();
//      response.result.addAll(getOrdersForTimeRange(fromTimestamp, lastOrderCreationTime));
//    }
//    return response.result;
//  }
//
//  protected TbOrder getOrderRecord(final _Order order) {
//    final Num price = getPrice(order);
//    final Num size = getSize(order);
//    return new TbOrder(
//        tradingPairUtils.parseSymbolToCommon(order.market),
//        Instant.parse(order.createdAt),
//        getStatus(order.status),
//        getOrderSide(order.side),
//        getType(order.type),
//        price,
//        size,
//        getOrderValue(price, size));
//  }
//
//  private Num getOrderValue(final Num price, final Num size) {
//    return price != null && size != null ? price.multipliedBy(size) : null;
//  }
//
//  private DecimalNum getSize(final _Order order) {
//    return order.filledSize == null ? null : DecimalNum.valueOf(order.filledSize);
//  }
//
//  private Num getPrice(final _Order order) {
//    return order.price != null
//        ? DecimalNum.valueOf(order.price)
//        : order.avgFillPrice != null ? DecimalNum.valueOf(order.avgFillPrice) : null;
//  }
//
//  private OrderType getType(final String type) {
//    return "market".equals(type) ? OrderType.MARKET : OrderType.LIMIT;
//  }
//
//  private OrderStatus getStatus(final String status) {
//    return switch (status) {
//      case "closed" -> OrderStatus.CLOSED;
//      case "open" -> OrderStatus.OPEN;
//      default -> OrderStatus.NEW;
//    };
//  }
//
//  private OrderSide getOrderSide(final String side) {
//    return "buy".equals(side) ? OrderSide.BUY : OrderSide.SELL;
//  }
//}
