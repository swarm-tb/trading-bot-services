package com.vigil.trading.history.mongo;


import com.vigil.trading.integration.models.order.SbOrderModel;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.time.Instant;
import java.util.List;

public interface SbOrdersRepository extends MongoRepository<SbOrderModel, String> {

  @Query("{ 'metaField': ?0, 'timestamp': { $gte: ?1, $lte: ?2 } }")
  List<SbOrderModel> findByMetaFieldAndTimestampBetween(
      String simulationId, Instant fromTimestamp, Instant toTimestamp, Sort sort
  );
}
