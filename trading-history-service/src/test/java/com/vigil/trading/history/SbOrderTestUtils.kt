package com.vigil.trading.history

import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.constant.order.OrderStatus
import com.vigil.trading.integration.constant.order.OrderType
import com.vigil.trading.integration.records.SbOrder
import com.vigil.trading.integration.utility.getValueForPercentage
import java.math.BigDecimal
import java.time.Instant

private var startTime = Instant.EPOCH
private val commissionPercentage = BigDecimal.valueOf(0.1)

fun getClosedMarketOrder(
    symbol: String, price: Double, size: Double, side: OrderSide
): SbOrder {
    val createdAt = startTime.plusSeconds(1)
    startTime = createdAt
    val priceNum = price.toBigDecimal()
    val sizeNum = size.toBigDecimal()
    val cost = priceNum * sizeNum
    val commission = cost.getValueForPercentage(commissionPercentage)
    return SbOrder(
        symbol,
        createdAt,
        OrderStatus.CLOSED,
        side,
        OrderType.MARKET,
        priceNum,
        sizeNum,
        cost,
        commission
    )
}
