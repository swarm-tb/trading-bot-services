package com.vigil.trading.history.container

import com.vigil.trading.history.getClosedMarketOrder
import com.vigil.trading.integration.constant.order.OrderSide
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class SymbolPositionsContainerTest {
    @Test
    fun shouldCorrectlyAccumulatePositions() {
        val symbolPositionsContainer = SymbolPositionsContainer()
        val o0 = getClosedMarketOrder("BTC-USD", 20000.0, 2.0, OrderSide.SELL)
        val o1 = getClosedMarketOrder("BTC-USD", 10000.0, 1.0, OrderSide.BUY)
        val o2 = getClosedMarketOrder("BTC-USD", 20000.0, 1.0, OrderSide.BUY)
        val o3 = getClosedMarketOrder("BTC-USD", 20000.0, 4.0, OrderSide.SELL)
        val o4 = getClosedMarketOrder("BTC-USD", 10000.0, 2.0, OrderSide.BUY)
        val o5 = getClosedMarketOrder("ETH-USD", 500.0, 3.0, OrderSide.BUY)
        val o6 = getClosedMarketOrder("ETH-USD", 1000.0, 3.0, OrderSide.SELL)
        symbolPositionsContainer.addPosition(o0)
        symbolPositionsContainer.addPosition(o1)
        symbolPositionsContainer.addPosition(o2)
        symbolPositionsContainer.addPosition(o3)
        symbolPositionsContainer.addPosition(o4)
        symbolPositionsContainer.addPosition(o5)
        symbolPositionsContainer.addPosition(o6)
        Assertions.assertThat(symbolPositionsContainer)
            .extracting { obj -> obj.totalCommission }
            .isEqualTo("174.500".toBigDecimal())
        Assertions.assertThat(symbolPositionsContainer)
            .extracting { obj -> obj.totalProfit }
            .isEqualTo("51325.500".toBigDecimal())
        Assertions.assertThat(symbolPositionsContainer.symbolPositions)
            .extractingByKey("BTC-USD")
            .extracting { obj -> obj.last }
            .extracting { obj -> obj.pNl }
            .isEqualTo("49830.000".toBigDecimal())
        Assertions.assertThat(symbolPositionsContainer.symbolPositions)
            .extractingByKey("ETH-USD")
            .extracting { obj -> obj.last }
            .extracting { obj -> obj.pNl }
            .isEqualTo("1495.500".toBigDecimal())
    }
}
