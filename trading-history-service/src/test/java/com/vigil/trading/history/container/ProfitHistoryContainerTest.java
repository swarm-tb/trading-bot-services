package com.vigil.trading.history.container;

import com.vigil.trading.integration.constant.order.OrderSide;
import com.vigil.trading.integration.dto.history.SbPositionHistoryRecord;
import com.vigil.trading.integration.records.TbProfit;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static java.math.BigDecimal.ZERO;
import static org.assertj.core.api.Assertions.assertThat;

class ProfitHistoryContainerTest {
  private Instant startTime = Instant.EPOCH;

  @Test
  void shouldCorrectlyFormHistory() {
    final SbPositionHistoryRecord p1 = getPosition(1);
    final SbPositionHistoryRecord p2 = getPosition(-2);
    final SbPositionHistoryRecord p3 = getPosition(10);
    final SbPositionHistoryRecord p4 = getPosition(-5);

    final ProfitHistoryContainer profitHistoryContainer = new ProfitHistoryContainer();
    profitHistoryContainer.add(p1);
    profitHistoryContainer.add(p2);
    profitHistoryContainer.add(p3);
    profitHistoryContainer.add(p4);

    assertThat(profitHistoryContainer.getProfitHistory())
        .hasSize(4)
        .last()
        .extracting(TbProfit::value)
        .isEqualTo(new BigDecimal("4.0"));
    assertThat(profitHistoryContainer.getMaxDrawdown()).isEqualByComparingTo(new BigDecimal("5"));
  }

  private SbPositionHistoryRecord getPosition(final double delta) {
    final Instant createdAt = startTime.plusSeconds(1);
    startTime = createdAt;
    return new SbPositionHistoryRecord(
        createdAt, OrderSide.BUY, ZERO, ZERO, ZERO, ZERO, ZERO, BigDecimal.valueOf(delta));
  }
}
