plugins {
    id("com.vigil.trading.common-dependencies") version "1.0.0"
}

version = "1.2.5"

dependencies {
    implementation("com.vigil.trading:integration:2.2.18")
    implementation("com.vigil.trading:binance-connector:2.0.4")

    implementation("org.apache.commons:commons-math3:3.6.1")

    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
}
