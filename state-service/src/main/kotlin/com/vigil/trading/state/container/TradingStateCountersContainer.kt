package com.vigil.trading.state.container

import java.util.concurrent.atomic.AtomicInteger

@JvmRecord
data class TradingStateCountersContainer(
    val shortPositionsCount: AtomicInteger = AtomicInteger(),
    val longPositionsCount: AtomicInteger = AtomicInteger(),
    val activePairsCount: AtomicInteger = AtomicInteger()
)
