package com.vigil.trading.state

import com.vigil.trading.integration.constant.Endpoint.Path.ACCOUNT_STATE
import com.vigil.trading.integration.constant.Endpoint.Path.REFRESH_STATE
import com.vigil.trading.integration.constant.Endpoint.PathVariableName.ACCOUNT
import com.vigil.trading.integration.constant.Endpoint.PathVariableName.SYMBOL
import com.vigil.trading.integration.constant.Endpoint.STATE
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.dto.state.SbTradingStateDto
import com.vigil.trading.integration.utility.logger
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(STATE)
class TradingStateController(private val tradingStateService: TradingStateService) {

    private val log by logger()

    @GetMapping(ACCOUNT_STATE)
    fun getTradingState(@PathVariable(ACCOUNT) account: SbAccount): SbTradingStateDto {
        log.info("Fetching trading state started")
        val tradingState = tradingStateService.getTradingState(account)
        log.info("Fetching trading state finished")
        return tradingState
    }

    @PutMapping(REFRESH_STATE)
    fun refreshLastChangeTimestamp(
        @PathVariable(ACCOUNT) account: SbAccount,
        @PathVariable(SYMBOL) symbol: String
    ) {
        tradingStateService.refreshLastChangeTimestamp(account, symbol)
    }
}
