package com.vigil.trading.state

import com.vigil.trading.integration.constant.SbAccount
import org.springframework.boot.context.properties.ConfigurationProperties
import java.math.BigDecimal
import java.time.Duration

@ConfigurationProperties(prefix = "state")
data class StateProperties(
    val telegramToken: String,
    val telegramChatId: String,
    val checkInterval: Duration,
    val activePairsPercentThreshold: BigDecimal,
    val inactivityThreshold: Duration,
    val account: Map<SbAccount, AccountProps>
) {
    fun getAccountProps(account: SbAccount): AccountProps? = this.account[account]
}

data class AccountProps(val enabled: Boolean)
