package com.vigil.trading.state.notification

import com.vigil.trading.integration.utility.logger
import com.vigil.trading.state.StateProperties
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.DefaultAbsSender
import org.telegram.telegrambots.bots.DefaultBotOptions
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.exceptions.TelegramApiException

@Component
class TbTelegramClient(private val stateProperties: StateProperties) :
    DefaultAbsSender(DefaultBotOptions(), stateProperties.telegramToken) {

    private val log by logger()

    fun sendMessage(message: String) {
        try {
            execute(
                SendMessage.builder().chatId(stateProperties.telegramChatId).text(message).build()
            )
        } catch (e: TelegramApiException) {
            log.error("Telegram exception: ", e)
        }
    }
}
