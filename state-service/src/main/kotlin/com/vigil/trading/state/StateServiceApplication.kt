package com.vigil.trading.state

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@EnableConfigurationProperties(StateProperties::class)
@SpringBootApplication(scanBasePackages = ["com.vigil.trading"])
open class StateServiceApplication

fun main(args: Array<String>) {
    runApplication<StateServiceApplication>(*args)
}
