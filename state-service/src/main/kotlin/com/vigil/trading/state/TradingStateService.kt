package com.vigil.trading.state

import com.vigil.trading.integration.clients.SymbolClient
import com.vigil.trading.integration.clients.TransactionClient
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.dto.state.SbTradingStateDto
import com.vigil.trading.integration.dto.state.SymbolStateDto
import com.vigil.trading.integration.records.SbSymbolInfo
import com.vigil.trading.integration.utility.getPercentageOf
import com.vigil.trading.state.container.TradingStateCountersContainer
import one.util.streamex.StreamEx
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.Duration
import java.time.Instant

@Service
class TradingStateService(
    private val transactionClient: TransactionClient,
    private val stateProperties: StateProperties,
    private val symbolClient: SymbolClient
) {
    private val accountSymbolsStateMap: Map<SbAccount, MutableMap<String, Instant>> = mapOf(
        SbAccount.SIMULATION to mutableMapOf(),
        SbAccount.META_API_DEMO to mutableMapOf(),
        SbAccount.BINANCE_USD_FUTURES to mutableMapOf(),
        SbAccount.META_API_FTMO to mutableMapOf(),
        SbAccount.META_API_FIDELCREST to mutableMapOf(),
        SbAccount.META_API_SURGE_TRADER to mutableMapOf()
    )

    private val defaultDeltaFromLastUpdate = Duration.ofHours(999)

    fun getTradingState(account: SbAccount): SbTradingStateDto {
        val countersContainer = TradingStateCountersContainer()
        val symbolsStateMap = accountSymbolsStateMap[account]!!
        val tradingPairStates = StreamEx.ofValues(symbolClient.getSymbolsInfo(account))
            .parallel()
            .map { symbolInfo -> getSymbolStateDto(symbolInfo, symbolsStateMap, countersContainer) }
            .toList()
        val currentTime = Instant.now()
        val accountInfo = transactionClient.getAccountInfo(account)
        return SbTradingStateDto(
            tradingPairStates,
            countersContainer.shortPositionsCount.get(),
            countersContainer.longPositionsCount.get(),
            countersContainer.activePairsCount.get(),
            accountInfo.currentDailyDrawdownPercent,
            accountInfo.drawdownFromStartPercent,
            accountInfo.profitPercent,
            currentTime
        )
    }

    fun getActivePairsPercent(account: SbAccount): BigDecimal {
        val symbolsInfo = symbolClient.getSymbolsInfo(account)
        val symbolsStateMap = accountSymbolsStateMap[account]!!
        if (symbolsStateMap.isEmpty()) {
            return BigDecimal.ZERO
        }
        val activePairsCount = StreamEx.ofValues(symbolsInfo)
            .parallel()
            .filter { symbolInfo ->
                isActive(
                    getDeltaFromLastUpdate(symbolsStateMap[symbolInfo.symbol()]),
                    symbolInfo.tradeSession()
                )
            }
            .count()
        return activePairsCount.toBigDecimal().getPercentageOf(symbolsStateMap.size.toBigDecimal())
    }

    fun refreshLastChangeTimestamp(account: SbAccount, symbol: String) {
        val symbolsStateMap = accountSymbolsStateMap[account]!!
        symbolsStateMap[symbol] = Instant.now()
    }

    private fun getSymbolStateDto(
        symbolInfo: SbSymbolInfo,
        symbolsStateMap: Map<String, Instant>,
        countersContainer: TradingStateCountersContainer
    ): SymbolStateDto {
        val symbol = symbolInfo.symbol()
        val minutesDeltaFromLastUpdate = getDeltaFromLastUpdate(symbolsStateMap[symbol]) ?: defaultDeltaFromLastUpdate
        val tradeSession = symbolInfo.tradeSession()
        val active = isActive(minutesDeltaFromLastUpdate, tradeSession)
        if (active) {
            countersContainer.activePairsCount.incrementAndGet()
        }
        val position = transactionClient.getPosition(symbolInfo.account(), symbol)
        incrementCounter(position.positionSide, countersContainer)
        return SymbolStateDto(symbol, minutesDeltaFromLastUpdate, position.positionSide, tradeSession, active)
    }

    private fun incrementCounter(
        positionSide: SbPositionSide,
        countersContainer: TradingStateCountersContainer
    ) {
        if (positionSide == SbPositionSide.LONG) {
            countersContainer.longPositionsCount.incrementAndGet()
        } else if (positionSide == SbPositionSide.SHORT) {
            countersContainer.shortPositionsCount.incrementAndGet()
        }
    }

    private fun getDeltaFromLastUpdate(lastUpdateTime: Instant?) =
        lastUpdateTime?.let { Duration.between(it, Instant.now()) }

    private fun isActive(deltaFromLastUpdate: Duration?, tradeSession: Boolean) =
        !tradeSession || deltaFromLastUpdate != null && stateProperties.inactivityThreshold > deltaFromLastUpdate
}
