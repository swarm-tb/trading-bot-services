package com.vigil.trading.state

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.state.notification.TbTelegramClient
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class TradingStateChecker(
    private val tradingStateService: TradingStateService,
    private val tbTelegramClient: TbTelegramClient,
    private val stateProperties: StateProperties
) {
    @Scheduled(
        fixedRateString = "#{beanFactory.getBean(T(com.vigil.trading.state.StateProperties)).checkInterval}",
        initialDelayString = "#{beanFactory.getBean(T(com.vigil.trading.state.StateProperties)).checkInterval}"
    )
    private fun checkStateAndNotify() {
        SbAccount.entries
            .filter { stateProperties.getAccountProps(it)?.enabled ?: false }
            .forEach { checkActivePairs(it) }
    }

    private fun checkActivePairs(account: SbAccount) {
        val activePairsPercent = tradingStateService.getActivePairsPercent(account)
        if (activePairsPercent < stateProperties.activePairsPercentThreshold) {
            val message = "$account, Trading is not stable: $activePairsPercent % active symbols"
            log.warn(message)
            tbTelegramClient.sendMessage(message)
        }
    }

    companion object {
        private val log by logger()
    }
}
