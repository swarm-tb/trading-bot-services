plugins {
    id("com.vigil.trading.common-dependencies") version "1.0.0"
}

version = "2.2.5"

dependencies {
    implementation("com.vigil.trading:integration:2.2.17")

    implementation("org.telegram:telegrambots:6.9.7.1") {
        exclude(group = "org.glassfish.jersey.core")
    }
}
