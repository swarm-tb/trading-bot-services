pluginManagement {
    repositories {
        gradlePluginPortal()
        maven {
            url = uri("https://gitlab.com/api/v4/groups/59954528/-/packages/maven")
        }
    }
}

rootProject.name = "transaction-service"
