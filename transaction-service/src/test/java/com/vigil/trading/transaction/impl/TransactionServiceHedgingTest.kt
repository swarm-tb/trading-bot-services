package com.vigil.trading.transaction.impl

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.constant.TradingAction
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbSymbolInfo
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.SbBalancingService
import com.vigil.trading.transaction.SbTransactionService
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.order.SbOrderFacade
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.BigDecimal.valueOf
import java.time.Instant

@SpringBootTest(classes = [TransactionServiceHedging::class])
internal class TransactionServiceHedgingTest {
    @Autowired
    lateinit var transactionService: SbTransactionService

    @MockBean
    lateinit var accountFacade: SbAccountFacade

    @MockBean
    lateinit var orderFacade: SbOrderFacade

    @MockBean
    lateinit var balancingService: SbBalancingService

    @BeforeEach
    fun setUp() {
        `when`(balancingService.getPositionCost(TEST_ACCOUNT, Instant.MIN)).thenReturn(POSITION_SIZE.toBigDecimal())
    }

    @Test
    fun shouldPlaceLongPositionWhenThereIsNoPosition() {
        val tradingActionInfo = getTradingActionInfo("20.00")
        transactionService.longPosition(tradingActionInfo)
        verify(orderFacade).makeMarketBuyOrder(tradingActionInfo, "10.00".toBigDecimal())
    }

    @Test
    fun shouldSwapPositionWhenActionIsOpposite() {
        val price = "20.00"
        val tradingActionInfo = getTradingActionInfo(price)
        val shortPosition = getShortPosition("5.00", price)
        `when`(accountFacade.getPosition(TEST_ACCOUNT, SYMBOL)).thenReturn(shortPosition).thenReturn(null)

        transactionService.longPosition(tradingActionInfo)

        verify(orderFacade).closePosition(tradingActionInfo, shortPosition)
        verify(orderFacade).makeMarketBuyOrder(tradingActionInfo, "10.00".toBigDecimal())
    }

    private fun getShortPosition(volume: String, openPrice: String): SbPosition {
        val volumeDecimal = volume.toBigDecimal()
        val openPriceDecimal = openPrice.toBigDecimal()
        return SbPosition(
            SYMBOL,
            SbPositionSide.SHORT,
            -volumeDecimal,
            volumeDecimal * openPriceDecimal,
            openPriceDecimal,
            null
        )
    }

    private fun getLongPosition(volume: Double, openPrice: Double): SbPosition {
        val volumeDecimal = valueOf(volume)
        val openPriceDecimal = valueOf(openPrice)
        return SbPosition(
            SYMBOL, SbPositionSide.LONG, volumeDecimal, volumeDecimal.multiply(openPriceDecimal), openPriceDecimal, null
        )
    }

    private fun getTradingActionInfo(
        price: String,
        contractSize: String = DEFAULT_CONTRACT_SIZE,
        minVolume: String = DEFAULT_MIN_VOLUME,
        maxVolume: String = DEFAULT_MAX_VOLUME
    ) = SbTradingActionInfo(
        getSymbolInfo(contractSize, minVolume, maxVolume), Instant.MIN, price.toBigDecimal(), TradingAction.NEUTRAL
    )

    companion object {
        private const val POSITION_SIZE = 200.0
        private const val DEFAULT_VOLUME_STEP = "0.01"
        private const val DEFAULT_MIN_VOLUME = "0.01"
        private const val DEFAULT_MAX_VOLUME = "10.0"
        private const val DEFAULT_CONTRACT_SIZE = "1.0"
        private const val SYMBOL = "TESTPERP"

        private val TEST_ACCOUNT = SbAccount.BINANCE_USD_FUTURES

        private fun getSymbolInfo(contractSize: String, minVolume: String, maxVolume: String) = SbSymbolInfo(
            SYMBOL,
            TEST_ACCOUNT,
            null,
            null,
            DEFAULT_VOLUME_STEP.toBigDecimal(),
            minVolume.toBigDecimal(),
            maxVolume.toBigDecimal(),
            contractSize.toBigDecimal(),
            null
        )
    }
}
