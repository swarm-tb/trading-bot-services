//package com.vigil.trading.ftx.service.impl;
//
//import com.vigil.trading.core.CredentialProperties;
//import com.vigil.trading.transaction.account.BinanceApiConfig;
//import io.contek.invoker.ftx.api.rest.user.PostOrders;
//import io.contek.invoker.ftx.api.rest.user.UserRestApi;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.boot.test.context.SpringBootTest;
//
//@EnableConfigurationProperties
//@SpringBootTest(classes = {BinanceApiConfig.class, CredentialProperties.class})
//class TradingPairServiceFtxImplTest  {
//
//  @Autowired
//  private UserRestApi userRestApi;
//
//  @Test
//  void test() {
//    final PostOrders.Response response =
//            userRestApi
//                    .postOrders()
//                    .setSide("buy")
//                    .setType("market")
//                    .setMarket("FTT/USD")
//                    .setSize(0.5)
//                    .submit();
//    System.out.println(response.result);
//  }
//}
