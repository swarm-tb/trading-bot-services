package com.vigil.trading.transaction.impl

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.constant.TradingAction
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbSymbolInfo
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.AccountProps
import com.vigil.trading.transaction.SbBalancingService
import com.vigil.trading.transaction.SbTransactionFacade
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.constant.SbAccountType
import com.vigil.trading.transaction.order.SbOrderFacade
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.BigDecimal
import java.math.BigDecimal.valueOf
import java.time.Instant

@SpringBootTest(classes = [SbTransactionFacade::class, TransactionServiceHedging::class, TransactionServiceNetting::class])
internal class TransactionServiceNettingTest {
    @Autowired
    lateinit var transactionFacade: SbTransactionFacade

    @MockBean
    lateinit var accountFacade: SbAccountFacade

    @MockBean
    lateinit var orderFacade: SbOrderFacade

    @MockBean
    lateinit var balancingService: SbBalancingService

    @MockBean
    lateinit var transactionProperties: TransactionProperties

    @Mock
    lateinit var accountProps: AccountProps

    @BeforeEach
    fun setUp() {
        `when`(balancingService.getPositionCost(TEST_ACCOUNT, null)).thenReturn(POSITION_SIZE)
        `when`(transactionProperties.getAccountProps(TEST_ACCOUNT)).thenReturn(accountProps)
        `when`(accountProps.accountType).thenReturn(SbAccountType.NETTING)
    }

    //  @Test
    //  void shouldCorrectLongPositionWhenLessThanHalfPositionCost() {
    //    final StrategyRecord strategyRecord = getStrategyRecord(20);
    //    transactionFacade.longPosition(strategyRecord);
    //    verify(orderFacade).makeMarketBuyOrder(strategyRecord, valueOf(10));
    //
    //    final StrategyRecord strategyRecord2 = getStrategyRecord(15);
    //    transactionFacade.longPosition(strategyRecord2);
    //    verify(orderFacade, times(1)).makeMarketBuyOrder(
    //        ArgumentMatchers.any(StrategyRecord.class),
    //        ArgumentMatchers.any(BigDecimal.class)
    //    );
    //
    //    final StrategyRecord strategyRecord3 = getStrategyRecord(8);
    //    transactionFacade.longPosition(strategyRecord3);
    //    verify(orderFacade).makeMarketBuyOrder(strategyRecord3, valueOf(15));
    //  }
    //
    //  @Test
    //  void shouldCorrectShortPositionWhenLessThanHalfPositionCost() {
    //    final StrategyRecord strategyRecord = getStrategyRecord(20);
    //    transactionFacade.shortPosition(strategyRecord);
    //    verify(orderFacade).makeMarketSellOrder(strategyRecord, valueOf(10));
    //
    //    final StrategyRecord strategyRecord2 = getStrategyRecord(25);
    //    transactionFacade.shortPosition(strategyRecord2);
    //    verify(orderFacade, times(1)).makeMarketSellOrder(
    //        ArgumentMatchers.any(StrategyRecord.class),
    //        ArgumentMatchers.any(BigDecimal.class)
    //    );
    //
    //    final StrategyRecord strategyRecord3 = getStrategyRecord(50);
    //    transactionFacade.shortPosition(strategyRecord3);
    //    verify(orderFacade).makeMarketBuyOrder(strategyRecord3, valueOf(6));
    //  }
    //
    //  @Test
    //  void shouldChangeToShortPosition() {
    //    final StrategyRecord strategyRecord = getStrategyRecord(20);
    //    transactionFacade.longPosition(strategyRecord);
    //    verify(orderFacade).makeMarketBuyOrder(strategyRecord, valueOf(10));
    //
    //    final StrategyRecord strategyRecord2 = getStrategyRecord(10);
    //    transactionFacade.shortPosition(strategyRecord2);
    //    verify(orderFacade).makeMarketSellOrder(strategyRecord2, valueOf(30));
    //  }
    //
    //  @Test
    //  void shouldOpenLongPosition() {
    //    accountFacade.updateSimulatedPositions(getClosedMarketOrder(TEST_PERP, 0, 0, OrderSide.BUY));
    //    final StrategyRecord strategyRecord = getStrategyRecord(20);
    //
    //    transactionFacade.longPosition(strategyRecord);
    //
    //    verify(orderFacade).makeMarketBuyOrder(strategyRecord, valueOf(10));
    //  }
    //
    //  @Test
    //  void shouldOpenShortPosition() {
    //    final StrategyRecord strategyRecord = getStrategyRecord(20);
    //    accountFacade.updateSimulatedPositions(getClosedMarketOrder(TEST_PERP, 0, 0, OrderSide.BUY));
    //
    //    transactionFacade.shortPosition(strategyRecord);
    //
    //    verify(orderFacade).makeMarketSellOrder(strategyRecord, valueOf(10));
    //  }
    //
    //  @Test
    //  void shouldChangeToLongPosition() {
    //    final StrategyRecord strategyRecord = getStrategyRecord(20);
    //    transactionFacade.shortPosition(strategyRecord);
    //    verify(orderFacade).makeMarketSellOrder(strategyRecord, valueOf(10));
    //
    //    final StrategyRecord strategyRecord2 = getStrategyRecord(40);
    //    transactionFacade.longPosition(strategyRecord2);
    //    verify(orderFacade).makeMarketBuyOrder(strategyRecord2, valueOf(15));
    //  }
    @Test
    fun shouldCloseLongPosition() {
        val openPrice = 50
        val tradingActionInfo = getTradingActionInfo(openPrice.toDouble())
        val volume = 0.05
        val volumeDecimal = valueOf(volume)
        `when`(accountFacade.getPosition(TEST_ACCOUNT, SYMBOL)).thenReturn(
            getLongPosition(
                volume, openPrice.toDouble()
            )
        )
        transactionFacade.closePosition(tradingActionInfo)
        verify(orderFacade).makeMarketSellOrder(tradingActionInfo, volumeDecimal)
    }

    @Test
    fun shouldCloseShortPosition() {
        val openPrice = 50
        val tradingActionInfo = getTradingActionInfo(openPrice.toDouble())
        val volume = 0.05
        val volumeDecimal = valueOf(volume)
        `when`(accountFacade.getPosition(TEST_ACCOUNT, SYMBOL)).thenReturn(
            getShortPosition(
                volume, openPrice.toDouble()
            )
        )
        transactionFacade.closePosition(tradingActionInfo)
        verify(orderFacade).makeMarketBuyOrder(tradingActionInfo, volumeDecimal)
    }

    private fun getShortPosition(volume: Double, openPrice: Double): SbPosition {
        val volumeDecimal = valueOf(volume)
        val openPriceDecimal = valueOf(openPrice)
        return SbPosition(
            SYMBOL,
            SbPositionSide.SHORT,
            volumeDecimal.negate(),
            volumeDecimal.multiply(openPriceDecimal),
            openPriceDecimal,
            null
        )
    }

    private fun getLongPosition(volume: Double, openPrice: Double): SbPosition {
        val volumeDecimal = valueOf(volume)
        val openPriceDecimal = valueOf(openPrice)
        return SbPosition(
            SYMBOL, SbPositionSide.LONG, volumeDecimal, volumeDecimal.multiply(openPriceDecimal), openPriceDecimal, null
        )
    }

    private fun getTradingActionInfo(price: Double): SbTradingActionInfo {
        return SbTradingActionInfo(symbolInfo, Instant.MIN, valueOf(price), TradingAction.NEUTRAL)
    }

    companion object {
        private val POSITION_SIZE = valueOf(200)
        private val TEST_ACCOUNT = SbAccount.BINANCE_USD_FUTURES
        private const val SYMBOL = "TESTPERP"
        private val symbolInfo: SbSymbolInfo
            get() = SbSymbolInfo(
                SYMBOL, TEST_ACCOUNT, null, null, null, null, null, BigDecimal.ONE, null
            )
    }
}
