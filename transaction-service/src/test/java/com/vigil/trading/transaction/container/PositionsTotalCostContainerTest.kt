package com.vigil.trading.transaction.container

import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.utility.HUNDRED
import com.vigil.trading.transaction.account.container.PositionsTotalCostContainer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.BigDecimal.valueOf

internal class PositionsTotalCostContainerTest {

    private val positionsTotalCostContainer = PositionsTotalCostContainer()

    @BeforeEach
    fun setUp() {
        positionsTotalCostContainer.clear()
    }

    @Test
    fun shouldCorrectlyCalculatePositionsDelta() {
        val initialDelta = getDelta(10.0, 10.0)
        val increasedPriceLongDelta = getDelta(10.0, 20.0)
        val invertSizeDelta = getDelta(-10.0, 20.0)
        val decreasePriceDelta = getDelta(-10.0, 10.0)
        val increasePriceShortDelta = getDelta(-10.0, 20.0)
        val increaseSizeAndPriceDelta = getDelta(20.0, 30.0)
        assertThat(initialDelta).isEqualByComparingTo(BigDecimal.ZERO)
        assertThat(increasedPriceLongDelta).isEqualByComparingTo(HUNDRED)
        assertThat(invertSizeDelta).isEqualByComparingTo(BigDecimal.ZERO)
        assertThat(decreasePriceDelta).isEqualByComparingTo(HUNDRED)
        assertThat(increasePriceShortDelta).isEqualByComparingTo(HUNDRED.negate())
        assertThat(increaseSizeAndPriceDelta).isEqualByComparingTo(HUNDRED.negate())
    }

    private fun getDelta(size: Double, price: Double) =
        positionsTotalCostContainer.getPositionDelta(getPosition(size, price), valueOf(price))

    companion object {

        private fun getPosition(volume: Double, price: Double) =
            // orderSide is not used in getPositionDelta but is required for SbPosition constructor
            SbPosition(
                TEST_PERP, SbPositionSide.CLOSED, valueOf(volume), valueOf(volume * price), valueOf(price), null
            )

        private const val TEST_PERP = "TEST-PERP"
    }
}
