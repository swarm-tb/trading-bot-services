package com.vigil.trading.transaction.utility;

import static com.vigil.trading.transaction.utility.RoundingUtils.getRoundedVolume;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RoundingUtilsTest {

  @Test
  @DisplayName("Round quantity when step size has scale")
  void getRoundedQuantity_scalePresent() {
    final BigDecimal roundedQuantity = getRoundedVolume(
        new BigDecimal("345.456576758"),
        new BigDecimal("0.001")
    );
    assertEquals(new BigDecimal("345.456"), roundedQuantity);
  }

  @Test
  @DisplayName("Round quantity when step size has no scale")
  void getRoundedQuantity_noScale() {
    final BigDecimal roundedQuantity = getRoundedVolume(
        new BigDecimal("345.456576758"),
        new BigDecimal("1")
    );
    assertEquals(new BigDecimal("345"), roundedQuantity);
  }
}
