package com.vigil.trading.transaction.repository.mongo;


import com.vigil.trading.integration.models.order.SbOrderModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SbOrdersRepository extends MongoRepository<SbOrderModel, String> {}
