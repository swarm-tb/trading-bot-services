package com.vigil.trading.transaction.account

import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.records.SbPosition
import java.time.Instant

interface SbAccountService {
    val positionsCache: Map<String, SbPosition>
    fun updatePositions()
    fun getAccountInfo(timestamp: Instant): SbAccountInfo
    fun getPosition(symbol: String): SbPosition?
    fun isAccountBalanceLimitReached(timestamp: Instant): Boolean
}
