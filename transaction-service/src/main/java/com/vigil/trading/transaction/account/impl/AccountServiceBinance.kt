package com.vigil.trading.transaction.account.impl

import com.fasterxml.jackson.core.JsonProcessingException
import com.vigil.trading.binance.BinanceAccountService
import com.vigil.trading.binance.PositionSide
import com.vigil.trading.binance.dto.BinancePosition
import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.clients.SymbolClient
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.isPositive
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.AbstractAccountService
import com.vigil.trading.transaction.constant.SbAccountType
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.time.Instant

@Component
class AccountServiceBinance(
    private val accountService: BinanceAccountService,
    private val symbolClient: SymbolClient,
    transactionProperties: TransactionProperties,
    commonProperties: CommonProperties,
    tradingActionKafkaTemplate: KafkaTemplate<String, SbTradingActionInfo>
) : AbstractAccountService(transactionProperties, commonProperties, tradingActionKafkaTemplate) {

    override val account = SbAccount.BINANCE_USD_FUTURES
    override val accountType = SbAccountType.NETTING

    override fun fetchPositions(): Map<String, SbPosition> =
        fetchAccountInfo().positions.associate { position ->
            position.symbol to updateLeverageAndGetPosition(position)
        }

    private fun updateLeverageAndGetPosition(binancePosition: BinancePosition): SbPosition {
        updateInitialLeverage(binancePosition)
        return getPosition(binancePosition)
    }

    private fun updateInitialLeverage(binancePosition: BinancePosition) {
        val expectedLeverage = transactionProperties.getAccountProps(account).leverage.toInt() + 1
        if (binancePosition.leverage > expectedLeverage) {
            log.warn(
                "Updating leverage for symbol {} from {} to {}",
                binancePosition.symbol,
                binancePosition.leverage,
                expectedLeverage
            )
            accountService.updateInitialPositionLeverage(binancePosition.symbol, expectedLeverage)
        }
    }

    override fun getAccountInfo(timestamp: Instant): SbAccountInfo {
        val equity = fetchAccountInfo().totalMarginBalance
        return SbAccountInfo(
            equity,
            equity,
            maxDrawdown.lastMaxDailyPercentage,
            maxDrawdown.maxTrailingPercentage,
            getProfitPercent(equity),
            symbolClient.getSymbolsCount(account)
        )
    }

    private fun fetchAccountInfo() =
        try {
            accountService.getAccountInfo()
        } catch (e: JsonProcessingException) {
            log.error("Error while fetching account info: {}", e.message)
            throw RuntimeException(e)
        }

    override fun getPriceForSymbolTimestamp(symbol: String, timestamp: Instant?) = null

    companion object {
        private fun getPosition(position: BinancePosition) =
            SbPosition(
                position.symbol,
                getOrderSide(position),
                position.positionAmt,
                position.entryPrice.multiply(position.positionAmt),
                position.entryPrice,
                null,
            )

        private fun getOrderSide(position: BinancePosition) =
            when (position.positionSide) {
                PositionSide.BOTH -> getSide(position.positionAmt)
                PositionSide.LONG -> SbPositionSide.LONG
                PositionSide.SHORT -> SbPositionSide.SHORT
                null -> throw RuntimeException("Position side is null")
            }

        private fun getSide(positionAmt: BigDecimal) =
            if (positionAmt.isPositive()) SbPositionSide.LONG else SbPositionSide.SHORT
    }
}
