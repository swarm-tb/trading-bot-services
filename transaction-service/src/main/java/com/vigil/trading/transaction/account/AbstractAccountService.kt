package com.vigil.trading.transaction.account

import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.TradingAction
import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbSymbolInfo
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.getDeltaPercentage
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.stat.MaxDrawdown
import com.vigil.trading.transaction.constant.SbAccountType
import one.util.streamex.StreamEx
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.scheduling.annotation.Scheduled
import java.math.BigDecimal
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

abstract class AbstractAccountService protected constructor(
    protected val transactionProperties: TransactionProperties,
    private val commonProperties: CommonProperties,
    private val tradingActionKafkaTemplate: KafkaTemplate<String, SbTradingActionInfo>
) : SbAccountService {

    var maxDrawdown = MaxDrawdown()

    protected val log by logger()

    override var positionsCache: Map<String, SbPosition> = ConcurrentHashMap()

    protected abstract val account: SbAccount

    protected abstract val accountType: SbAccountType

    @Scheduled(
        fixedRateString = "#{beanFactory.getBean(T(com.vigil.trading.transaction.TransactionProperties)).accountUpdateInterval}"
    )
    fun updatePositionsScheduled() {
        try {
            if (account == SbAccount.SIMULATION || !transactionProperties.getAccountProps(account).transactionsEnabled) {
                log.debug(
                    "Positions won't be updated. Account updates are disabled for account {}", account
                )
                return
            }
            updatePositions()
            closeAllPositionsIfLimitReached(Instant.now())
        } catch (e: Exception) {
            log.error("Account: {}, Exception during scheduled account info update: {}", account, e.message)
            log.debug("Stack trace:", e)
        }
    }

    override fun getPosition(symbol: String) = positionsCache[symbol]

    override fun updatePositions() {
        synchronized(this) {
            positionsCache = fetchPositions()
        }
    }

    protected abstract fun fetchPositions(): Map<String, SbPosition>

    protected fun closeAllPositionsIfLimitReached(timestamp: Instant) {
        val accountInfo = getAccountInfo(timestamp)
        val maxEquityBalance = maxEquityBalance(accountInfo)
        maxDrawdown.processTrailing(maxEquityBalance, timestamp)
        maxDrawdown.processDaily(maxEquityBalance, timestamp)
        if (positionsCache.isNotEmpty() && isAccountBalanceLimitReached(maxEquityBalance, timestamp)) {
            closeAllPositions(timestamp)
        }
    }

    override fun isAccountBalanceLimitReached(timestamp: Instant): Boolean {
        val accountInfo = getAccountInfo(timestamp)
        return isAccountBalanceLimitReached(maxEquityBalance(accountInfo), timestamp)
    }

    private fun maxEquityBalance(accountInfo: SbAccountInfo) = accountInfo.equity.max(accountInfo.balance)

    private fun isAccountBalanceLimitReached(equity: BigDecimal, timestamp: Instant) =
        isDailyDrawdownLimitReached(timestamp) || isProfitTargetReached(timestamp, equity)

    private fun isDailyDrawdownLimitReached(timestamp: Instant): Boolean {
        val lastDailyPercentage = maxDrawdown.lastMaxDailyPercentage
        val maxDailyLossPercent = transactionProperties.getAccountProps(account).maxDailyLossPercent
        val result = lastDailyPercentage > maxDailyLossPercent
        if (result) {
            log.warn(
                "Account: {}, {}, Max daily loss percent violated: {} > {}",
                account,
                timestamp,
                lastDailyPercentage,
                maxDailyLossPercent
            )
        }
        return result
    }

    private fun isProfitTargetReached(timestamp: Instant, equity: BigDecimal): Boolean {
        val delta = getProfitPercent(equity)
        val profitTargetPercent = transactionProperties.getAccountProps(account).profitTargetPercent
        val result = delta >= profitTargetPercent
        if (result) {
            log.warn(
                "Account: {}, {}, Profit target reached: {} >= {}",
                account,
                timestamp,
                delta,
                profitTargetPercent
            )
        }
        return result
    }

    protected fun getProfitPercent(equity: BigDecimal): BigDecimal =
        transactionProperties.getAccountProps(account).initialBalance.getDeltaPercentage(equity)

    private fun closeAllPositions(timestamp: Instant?) =
        StreamEx.ofValues(positionsCache).parallel().forEach { position -> closePosition(position, timestamp) }

    protected fun closePosition(position: SbPosition, timestamp: Instant?) {
        tradingActionKafkaTemplate.send(
            commonProperties.kafka.tradingActionsTopic,
            getCloseTradingActionInfo(position, timestamp)
        )
    }

    private fun getCloseTradingActionInfo(position: SbPosition, timestamp: Instant?) =
        SbTradingActionInfo(
            getSymbolInfo(position),
            //TODO it should be a separate action or even direct call
            timestamp,
            getPriceForSymbolTimestamp(position.symbol, timestamp),
            TradingAction.CLOSE
        )

    protected abstract fun getPriceForSymbolTimestamp(symbol: String, timestamp: Instant?): BigDecimal?

    private fun getSymbolInfo(position: SbPosition) =
        SbSymbolInfo(
            position.symbol, account, null, null, null, null, null, null, null
        )
}
