package com.vigil.trading.transaction.account

import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.clients.SymbolClient
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.metaapi.MetaApiAccount
import com.vigil.trading.integration.metaapi.MetaApiAccountServiceGrpc
import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.constant.SbAccountType
import org.springframework.kafka.core.KafkaTemplate
import java.math.BigDecimal
import java.math.BigDecimal.valueOf
import java.time.Instant

abstract class AbstractMetaApiAccountService protected constructor(
    private val accountServiceBlockingStub: MetaApiAccountServiceGrpc.MetaApiAccountServiceBlockingStub,
    private val symbolClient: SymbolClient,
    transactionProperties: TransactionProperties,
    commonProperties: CommonProperties,
    tradingActionKafkaTemplate: KafkaTemplate<String, SbTradingActionInfo>
) : AbstractAccountService(transactionProperties, commonProperties, tradingActionKafkaTemplate) {

    protected abstract val accountId: String?

    override val accountType = SbAccountType.HEDGING

    override fun fetchPositions(): Map<String, SbPosition> {
        val positions = accountServiceBlockingStub.getPositions(
            MetaApiAccount.MetaApiAccountInfoRequest.newBuilder().setAccountId(accountId).build()
        ).positionsList
        return positions.associate { position -> position.symbol to getPosition(position) }
    }

    override fun getAccountInfo(timestamp: Instant): SbAccountInfo {
        val accountInfo = accountServiceBlockingStub.getAccountInfo(
            MetaApiAccount.MetaApiAccountInfoRequest.newBuilder().setAccountId(accountId).build()
        )
        val symbolsCount = symbolClient.getSymbolsCount(account)
        val equity = valueOf(accountInfo.equity)
        return SbAccountInfo(
            equity,
            valueOf(accountInfo.balance),
            maxDrawdown.lastMaxDailyPercentage,
            maxDrawdown.maxTrailingPercentage,
            getProfitPercent(equity),
            symbolsCount
        )
    }

    override fun getPriceForSymbolTimestamp(symbol: String, timestamp: Instant?) = null

    companion object {

        private fun getPosition(position: MetaApiAccount.MetaApiPosition): SbPosition {
            val positionType = position.type
            val volume = getVolume(valueOf(position.volume), positionType)
            return SbPosition(
                position.symbol,
                getSide(positionType),
                volume,
                null,
                null,
                null,
            )
        }

        private fun getVolume(volume: BigDecimal, type: MetaApiAccount.PositionType) =
            if (type == MetaApiAccount.PositionType.POSITION_TYPE_BUY) volume else volume.negate()

        private fun getSide(type: MetaApiAccount.PositionType) =
            if (type == MetaApiAccount.PositionType.POSITION_TYPE_BUY) SbPositionSide.LONG else SbPositionSide.SHORT
    }
}
