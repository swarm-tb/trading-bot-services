package com.vigil.trading.transaction.account

import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.TbProfit
import com.vigil.trading.transaction.account.stat.MaxDrawdown
import java.math.BigDecimal
import java.util.*

interface SbAccountServiceSimulation : SbAccountService {
    fun clearCache()
    fun updatePosition(position: SbPosition, commission: BigDecimal)
    val equityHistory: SortedSet<TbProfit>
    var maxDrawdown: MaxDrawdown
    val equity: BigDecimal
}
