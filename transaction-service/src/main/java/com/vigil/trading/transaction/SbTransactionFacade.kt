package com.vigil.trading.transaction

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.constant.SbAccountType
import com.vigil.trading.transaction.impl.TransactionServiceHedging
import com.vigil.trading.transaction.impl.TransactionServiceNetting
import org.springframework.stereotype.Service

@Service
class SbTransactionFacade(
    transactionServiceHedging: TransactionServiceHedging,
    transactionServiceNetting: TransactionServiceNetting,
    private val accountFacade: SbAccountFacade,
    private val transactionProperties: TransactionProperties
) {
    private val transactionStrategyMap: Map<SbAccountType, SbTransactionService> = mapOf(
        SbAccountType.HEDGING to transactionServiceHedging,
        SbAccountType.NETTING to transactionServiceNetting
    )

    fun longPosition(tradingActionInfo: SbTradingActionInfo) {
        if (accountFacade.isAccountBalanceLimitReached(tradingActionInfo)) {
            logWarning(tradingActionInfo)
            return
        }
        transactionStrategyMap[getAccountType(tradingActionInfo.symbolInfo.account)]!!.longPosition(tradingActionInfo)
    }

    fun shortPosition(tradingActionInfo: SbTradingActionInfo) {
        if (accountFacade.isAccountBalanceLimitReached(tradingActionInfo)) {
            logWarning(tradingActionInfo)
            return
        }
        transactionStrategyMap[getAccountType(tradingActionInfo.symbolInfo.account)]!!.shortPosition(tradingActionInfo)
    }

    fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        transactionStrategyMap[getAccountType(tradingActionInfo.symbolInfo.account)]!!.closePosition(tradingActionInfo)
    }

    fun closePositions(account: SbAccount, positionSide: SbPositionSide?) {
        if (!transactionProperties.getAccountProps(account).transactionsEnabled) {
            log.debug("Can't close positions, transactions are disabled for {}", account)
            return
        }
        transactionStrategyMap[getAccountType(account)]!!.closePositions(account, positionSide)
    }

    private fun getAccountType(account: SbAccount): SbAccountType {
        return transactionProperties.getAccountProps(account).accountType
    }

    companion object {
        private val log by logger()

        private fun logWarning(tradingActionInfo: SbTradingActionInfo) {
            log.warn(
                "Account: {}, limit reached, skipping {} position for {}",
                tradingActionInfo.symbolInfo.account(),
                tradingActionInfo.tradingAction,
                tradingActionInfo.symbolInfo.symbol()
            )
        }
    }
}
