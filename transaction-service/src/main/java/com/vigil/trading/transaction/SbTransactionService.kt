package com.vigil.trading.transaction

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbTradingActionInfo

interface SbTransactionService {
    fun longPosition(tradingActionInfo: SbTradingActionInfo)
    fun shortPosition(tradingActionInfo: SbTradingActionInfo)
    fun closePosition(tradingActionInfo: SbTradingActionInfo)
    fun closePositions(account: SbAccount, positionSide: SbPositionSide?)
}
