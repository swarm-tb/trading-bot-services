package com.vigil.trading.transaction.account.impl

import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.records.TbProfit
import com.vigil.trading.integration.records.simulation.SbInstantContext
import com.vigil.trading.integration.records.simulation.SbSymbolContext
import com.vigil.trading.integration.utility.isPositive
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.AbstractAccountService
import com.vigil.trading.transaction.account.SbAccountServiceSimulation
import com.vigil.trading.transaction.account.container.PositionsTotalCostContainer
import com.vigil.trading.transaction.account.stat.MaxDrawdown
import com.vigil.trading.transaction.constant.SbAccountType
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Component
class AccountServiceSimulation(
    transactionProperties: TransactionProperties,
    commonProperties: CommonProperties,
    tradingActionKafkaTemplate: KafkaTemplate<String, SbTradingActionInfo>
) : AbstractAccountService(transactionProperties, commonProperties, tradingActionKafkaTemplate),
    SbAccountServiceSimulation {

    private val positionsTotalCostContainer = PositionsTotalCostContainer()

    private val instantContextMap = HashMap<Instant, Map<String, SbSymbolContext>>()

    private var mutablePositionsCache: MutableMap<String, SbPosition> = ConcurrentHashMap()

    override var positionsCache: Map<String, SbPosition>
        get() = mutablePositionsCache
        set(value) {
            mutablePositionsCache = value as ConcurrentHashMap<String, SbPosition>
        }

    override val account = SbAccount.SIMULATION
    override val accountType = SbAccountType.HEDGING

    override var equity = transactionProperties.getAccountProps(account).initialBalance
    override val equityHistory = TreeSet<TbProfit>()

    @KafkaListener(
        topics = ["#{commonProperties.getKafka().getInstantContextTopic()}"],
        groupId = "#{commonProperties.getKafka().getAccountServiceGroupId()}"
    )
    fun consumeCandleInfo(instantContext: SbInstantContext) {
        instantContextMap[instantContext.timestamp()] = instantContext.symbolsContext()
    }

    @KafkaListener(
        topics = ["#{commonProperties.getKafka().getAccountUpdateTopic()}"],
        groupId = "#{commonProperties.getKafka().getAccountServiceGroupId()}"
    )
    fun consumeAccountUpdate(timestamp: Instant) {
        val instantContextMap = instantContextMap[timestamp]!!
        val positionsTotalDelta = mutablePositionsCache.values.fold(BigDecimal.ZERO) { totalValue, position ->
            increment(totalValue, position, instantContextMap)
        }
        equity = equity.add(positionsTotalDelta)
        equityHistory.add(TbProfit(timestamp, equity))
        closeAllPositionsIfLimitReached(timestamp)
        closePositionsViolatedStopLoss(instantContextMap, timestamp)
    }

    private fun closePositionsViolatedStopLoss(
        instantContextMap: Map<String, SbSymbolContext>,
        timestamp: Instant
    ) {
        positionsCache.values.forEach { position ->
            val price = instantContextMap[position.symbol]?.price ?: run {
                log.warn("No price data available for symbol: ${position.symbol}")
                return@forEach
            }
            when (position.positionSide) {
                SbPositionSide.LONG -> if (price <= position.stopLossPrice) logAndClosePosition(
                    position,
                    price,
                    timestamp
                )

                SbPositionSide.SHORT -> if (price >= position.stopLossPrice) logAndClosePosition(
                    position,
                    price,
                    timestamp
                )
                else -> log.trace("Position stop loss verification skipped for CLOSE order side")
            }
        }
    }

    private fun logAndClosePosition(
        position: SbPosition,
        price: BigDecimal,
        timestamp: Instant
    ) {
        log.warn(
            "Position stop loss violated for symbol: {}, Open Price: {}, Current Price: {}, Stop loss: {}",
            position.symbol,
            position.openPrice,
            price,
            position.stopLossPrice
        )
        closePosition(position, timestamp)
    }

    override fun clearCache() {
        mutablePositionsCache.clear()
        positionsTotalCostContainer.clear()
        equityHistory.clear()
        maxDrawdown = MaxDrawdown()
        instantContextMap.clear()
        equity = transactionProperties.getAccountProps(account).initialBalance
    }

    override fun updatePosition(position: SbPosition, commission: BigDecimal) {
        if (position.positionSide == SbPositionSide.CLOSED) {
            mutablePositionsCache.remove(position.symbol)
            return
        }
        val computeSize: (BigDecimal, BigDecimal) -> BigDecimal
        val defaultPosition: SbPosition
        val cost = position.cost!!
        if (position.positionSide == SbPositionSide.LONG) {
            computeSize = BigDecimal::add
            defaultPosition = SbPosition(
                position.symbol,
                SbPositionSide.LONG,
                position.volume,
                cost,
                position.openPrice,
                position.stopLossPrice
            )
        } else {
            computeSize = BigDecimal::subtract
            defaultPosition =
                SbPosition(
                    position.symbol,
                    SbPositionSide.SHORT,
                    -position.volume,
                    -cost,
                    position.openPrice,
                    position.stopLossPrice
                )
        }
        processPosition(position, computeSize, defaultPosition)
        equity = equity.subtract(commission)
    }

    override fun getAccountInfo(timestamp: Instant) =
        SbAccountInfo(
            equity,
            equity,
            maxDrawdown.lastMaxDailyPercentage,
            maxDrawdown.maxTrailingPercentage,
            getProfitPercent(equity),
            instantContextMap[timestamp]!!.size
        )

    private fun increment(
        totalValue: BigDecimal, position: SbPosition, instantContextMap: Map<String, SbSymbolContext>
    ): BigDecimal {
        val tradingActionInfo = instantContextMap[position.symbol]
        val price = tradingActionInfo?.price()
        return totalValue.add(price?.let { getPositionsDelta(position, it) } ?: BigDecimal.ZERO)
    }

    private fun getPositionsDelta(position: SbPosition, price: BigDecimal): BigDecimal {
        return positionsTotalCostContainer.getPositionDelta(position, price)
    }

    private fun processPosition(
        newPosition: SbPosition,
        computeSize: (BigDecimal, BigDecimal) -> BigDecimal,
        defaultPosition: SbPosition
    ) {
        val oldPosition = getPosition(newPosition.symbol)
        val updatedPosition =
            oldPosition?.let { existingOldPosition ->
                updatePosition(
                    newPosition,
                    existingOldPosition,
                    computeSize
                )
            }
                ?: defaultPosition
        mutablePositionsCache[defaultPosition.symbol] = updatedPosition
    }

    private fun updatePosition(
        newPosition: SbPosition,
        oldPosition: SbPosition,
        computeSize: (BigDecimal, BigDecimal) -> BigDecimal
    ): SbPosition {
        val newPositionValue = computeSize(oldPosition.volume, newPosition.volume)
        val orderSide = if (newPositionValue.isPositive()) SbPositionSide.LONG else SbPositionSide.SHORT
        return SbPosition(
            newPosition.symbol,
            orderSide,
            newPositionValue,
            newPositionValue.multiply(newPosition.openPrice),
            newPosition.openPrice,
            newPosition.stopLossPrice
        )
    }

    override fun getPriceForSymbolTimestamp(symbol: String, timestamp: Instant?): BigDecimal? {
        return instantContextMap[timestamp]!![symbol]!!.price()
    }

    override fun fetchPositions(): Map<String, SbPosition> = throw UnsupportedOperationException()
}
