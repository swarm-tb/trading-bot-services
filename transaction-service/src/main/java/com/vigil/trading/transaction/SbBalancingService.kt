package com.vigil.trading.transaction

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.utility.div
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.transaction.account.SbAccountFacade
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.Instant

@Service
class SbBalancingService(
    private val accountFacade: SbAccountFacade,
    private val transactionProperties: TransactionProperties
) {
    fun getPositionCost(account: SbAccount, timestamp: Instant?): BigDecimal {
        val accountInfo = accountFacade.getAccountInfo(account, timestamp!!)
        val positionCost = getPositionCost(accountInfo, account)
        log.debug("Account info: {}, Position cost: {}", accountInfo, positionCost)
        return positionCost
    }

    private fun getPositionCost(accountInfo: SbAccountInfo, account: SbAccount): BigDecimal {
        return accountInfo.equity / accountInfo.activeSymbolsCount *
                transactionProperties.getAccountProps(account).leverage
    }

    companion object {
        private val log by logger()
    }
}
