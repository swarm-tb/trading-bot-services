package com.vigil.trading.transaction.exception;

import com.vigil.trading.integration.constant.SbAccount;

import java.math.BigDecimal;

public class TransactionException extends RuntimeException {

  public TransactionException(final String message) {
    super(message);
  }

  public static class MinSizeException extends TransactionException {

    public MinSizeException(
        final SbAccount account,
        final String symbol,
        final BigDecimal quantity,
        final BigDecimal minSize
    ) {
      super(
          "The quantity %s for account %s symbol %s is less than the minimum size %s"
              .formatted(quantity, account, symbol, minSize)
      );
    }
  }
}
