package com.vigil.trading.transaction;

import com.vigil.trading.integration.constant.Endpoint;
import com.vigil.trading.integration.constant.Endpoint.Path;
import com.vigil.trading.integration.constant.Endpoint.PathVariableName;
import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.dto.history.AccountBalanceHistory;
import com.vigil.trading.integration.records.SbAccountInfo;
import com.vigil.trading.integration.records.SbPosition;
import com.vigil.trading.transaction.account.SbAccountFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping(Endpoint.ACCOUNT)
public class AccountController {

  private final SbAccountFacade accountFacade;

  public AccountController(final SbAccountFacade accountFacade) {
    this.accountFacade = accountFacade;
  }

  @GetMapping(Path.POSITION)
  public SbPosition getPosition(
      @PathVariable(PathVariableName.ACCOUNT) final SbAccount account,
      @PathVariable(PathVariableName.SYMBOL) final String symbol
  ) {
    return accountFacade.getPosition(account, symbol);
  }

  @GetMapping(Path.ACCOUNT_INFO)
  public SbAccountInfo getAccountInfo(@PathVariable(PathVariableName.ACCOUNT) final SbAccount account) {
    return accountFacade.getAccountInfo(account, Instant.now());
  }

  @GetMapping(Path.ACCOUNTS_LIST)
  public List<SbAccount> getAccounts() {
    return SbAccount.getEntries();
  }

  @GetMapping(Path.BALANCE_HISTORY)
  public AccountBalanceHistory getAccountBalanceHistory() {
    return accountFacade.getAccountBalanceHistory();
  }
}
