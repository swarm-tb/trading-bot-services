package com.vigil.trading.transaction

import com.vigil.trading.integration.constant.Endpoint
import com.vigil.trading.integration.constant.Endpoint.Path
import com.vigil.trading.integration.constant.Endpoint.PathVariableName
import com.vigil.trading.integration.constant.Endpoint.RequestParamName
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal

@RestController
@RequestMapping(Endpoint.TRANSACTION)
class TransactionController(
    private val transactionProperties: TransactionProperties,
    private val transactionFacade: SbTransactionFacade
) {
    @PutMapping(Path.LEVERAGE)
    fun setLeverage(
        @PathVariable(PathVariableName.ACCOUNT) account: SbAccount,
        @RequestParam(RequestParamName.LEVERAGE) leverage: BigDecimal
    ): BigDecimal {
        transactionProperties.getAccountProps(account).leverage = leverage
        return leverage
    }

    @GetMapping(Path.LEVERAGE)
    fun getLeverage(@PathVariable(PathVariableName.ACCOUNT) account: SbAccount): BigDecimal {
        return transactionProperties.getAccountProps(account).leverage
    }

    @PutMapping(Path.CLOSE_POSITIONS)
    fun closePositions(
        @PathVariable(PathVariableName.ACCOUNT) account: SbAccount,
        @RequestParam(RequestParamName.POSITION_SIDE) positionSide: SbPositionSide?
    ) {
        transactionFacade.closePositions(account, positionSide)
    }
}
