package com.vigil.trading.transaction.order.impl

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.constant.order.OrderStatus
import com.vigil.trading.integration.constant.order.OrderType
import com.vigil.trading.integration.models.order.SbOrderModel
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.getValueForPercentage
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.order.AbstractOrderService
import com.vigil.trading.transaction.repository.mongo.SbOrdersRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class OrdersServiceSimulation(
    private val accountFacade: SbAccountFacade,
    private val ordersRepository: SbOrdersRepository,
    transactionProperties: TransactionProperties
) : AbstractOrderService(transactionProperties) {

    override val account = SbAccount.SIMULATION

    override fun makeMarketBuyOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        val stopLossPrice = tradingActionInfo.price!! * stopLossPriceCoefficient
        makeOrder(tradingActionInfo, OrderSide.BUY, quantity, stopLossPrice)
    }

    override fun makeMarketSellOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        val stopLossPrice = tradingActionInfo.price!! / stopLossPriceCoefficient
        makeOrder(tradingActionInfo, OrderSide.SELL, quantity, stopLossPrice)
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo, position: SbPosition) {
        makeOrder(tradingActionInfo, OrderSide.CLOSE, position.volume, BigDecimal.ZERO)
    }

    override fun closePosition(position: SbPosition) {
        TODO("Not yet implemented")
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val account = symbolInfo.account()
        val symbol = symbolInfo.symbol()
        val position = accountFacade.getPosition(account, symbol)
        if (position == null) {
            log.warn("Account: {}, Symbol: {}, No position to close.", account, symbol)
            return
        }
        closePosition(tradingActionInfo, position)
    }

    private fun makeOrder(
        tradingActionInfo: SbTradingActionInfo, side: OrderSide, quantity: BigDecimal, stopLossPrice: BigDecimal
    ) {
        log.trace(
            "Market order, Pair: {}, Side: {}, Quantity: {}", tradingActionInfo, side, quantity
        )
        val commission =
            quantity.getValueForPercentage(transactionProperties.commissionPercent) * tradingActionInfo.price!!
        val costWithoutCommission = tradingActionInfo.price!! * quantity
        accountFacade.updatePositions(
            getPosition(tradingActionInfo, side, quantity, costWithoutCommission, stopLossPrice),
            commission
        )
        ordersRepository.save(getOrderModel(tradingActionInfo, side, quantity, costWithoutCommission, commission))
    }

    private fun getOrderModel(
        tradingActionInfo: SbTradingActionInfo,
        side: OrderSide,
        size: BigDecimal,
        cost: BigDecimal,
        commission: BigDecimal
    ) = SbOrderModel(
        tradingActionInfo.timestamp!!,
        transactionProperties.simulationId,
        tradingActionInfo.symbolInfo.symbol(),
        OrderStatus.CLOSED,
        side,
        OrderType.MARKET,
        tradingActionInfo.price!!,
        size,
        cost,
        commission
    )

    private fun getPosition(
        tradingActionInfo: SbTradingActionInfo,
        side: OrderSide,
        size: BigDecimal,
        cost: BigDecimal,
        stopLossPrice: BigDecimal
    ): SbPosition {
        val price = tradingActionInfo.price
        return SbPosition(
            tradingActionInfo.symbolInfo.symbol(), getPositionSide(side), size, cost, price, stopLossPrice
        )
    }

    private fun getPositionSide(side: OrderSide) = when (side) {
        OrderSide.BUY -> SbPositionSide.LONG
        OrderSide.SELL -> SbPositionSide.SHORT
        else -> throw IllegalArgumentException("Wrong order side: $side")
    }
}
