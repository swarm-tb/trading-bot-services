package com.vigil.trading.transaction.impl

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.isNegative
import com.vigil.trading.integration.utility.isPositive
import com.vigil.trading.transaction.AbstractTransactionService
import com.vigil.trading.transaction.SbBalancingService
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.order.SbOrderFacade
import org.springframework.stereotype.Service
import java.math.BigDecimal


@Service
class TransactionServiceHedging(
    orderFacade: SbOrderFacade,
    accountFacade: SbAccountFacade,
    balancingService: SbBalancingService
) : AbstractTransactionService(orderFacade, accountFacade, balancingService) {

    override fun longPosition(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val position = accountFacade.getPosition(symbolInfo.account(), symbolInfo.symbol())
        if (position == null) {
            orderFacade.makeMarketBuyOrder(tradingActionInfo, getExpectedPositionVolume(tradingActionInfo))
            return
        }
        val currentPositionVolume = position.volume
        if (currentPositionVolume.isNegative() || isVolumeDeltaGreaterThanThreshold(
                getExpectedPositionVolume(tradingActionInfo),
                currentPositionVolume,
                BigDecimal::subtract
            )
        ) {
            orderFacade.closePosition(tradingActionInfo, position)
            longPosition(tradingActionInfo)
        }
    }

    override fun shortPosition(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val position = accountFacade.getPosition(symbolInfo.account(), symbolInfo.symbol())
        if (position == null) {
            orderFacade.makeMarketSellOrder(tradingActionInfo, getExpectedPositionVolume(tradingActionInfo))
            return
        }
        val currentPositionVolume = position.volume
        if (currentPositionVolume.isPositive() || isVolumeDeltaGreaterThanThreshold(
                getExpectedPositionVolume(tradingActionInfo),
                currentPositionVolume,
                BigDecimal::add
            )
        ) {
            orderFacade.closePosition(tradingActionInfo, position)
            shortPosition(tradingActionInfo)
        }
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        orderFacade.closePosition(tradingActionInfo)
    }

    override fun closePositions(account: SbAccount, positionSide: SbPositionSide?) {
        TODO("Not yet implemented")
    }
}
