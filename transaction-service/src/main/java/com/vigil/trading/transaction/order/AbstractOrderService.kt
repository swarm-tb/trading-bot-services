package com.vigil.trading.transaction.order

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.transaction.TransactionProperties
import java.math.BigDecimal

abstract class AbstractOrderService protected constructor(
    protected val transactionProperties: TransactionProperties
) : SbOrderService {

    protected abstract val account: SbAccount
    protected val log by logger()

    protected fun logOrder(
        side: OrderSide,
        symbol: String,
        size: BigDecimal,
        stopLossPrice: BigDecimal
    ) {
        log.info(
            "Making order, Account: {}, Side: {}, Symbol: {}, Size: {}, Stop loss price: {}",
            account,
            side,
            symbol,
            size,
            stopLossPrice
        )
    }

    protected val stopLossPriceCoefficient: BigDecimal
        get() = BigDecimal.ONE.subtract(transactionProperties.getAccountProps(account).positionStopLossFraction)
}
