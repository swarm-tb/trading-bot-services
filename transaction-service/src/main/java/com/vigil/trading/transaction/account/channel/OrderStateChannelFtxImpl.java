//package com.vigil.trading.transaction.account.channel;
//
//
//import com.vigil.trading.transaction.account.AccountInfoCache;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Profile;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.PostConstruct;
//
//import static com.vigil.trading.integration.constant.Profiles.BINANCE;
//
//@Slf4j
//@Service
//@Profile(BINANCE)
//public class OrderStateChannelFtxImpl
//    implements ISubscribingConsumer<OrdersChannel.Message> {
//  private final AccountBalanceCache accountBalanceCache;
//  private final UserWebSocketApi userWebSocketApi;
//  private final AccountInfoCache accountInfoCache;
//
//  public OrderStateChannelFtxImpl(
//          final AccountBalanceCache accountBalanceCache,
//          final UserWebSocketApi userWebSocketApi,
//          final AccountInfoCache accountInfoCache
//  ){
//    this.accountBalanceCache = accountBalanceCache;
//    this.userWebSocketApi = userWebSocketApi;
//    this.accountInfoCache = accountInfoCache;
//  }
//
//  @PostConstruct
//  private void postConstruct() {
//    log.info("Opening order state channel");
//    userWebSocketApi.getOrderUpdateChannel().addConsumer(this);
//  }
//
//  @Override
//  public void onNext(final OrdersChannel.Message message) {
//    Thread.currentThread().setName(getClass().getSimpleName());
//    log.debug("Symbol: {}, Status: {}", message.data.market, message.data.status);
//    accountBalanceCache.updateBalances();
//    accountInfoCache.updatePositions(null);
//  }
//
//  @Override
//  public void onStateChange(final SubscriptionState state) {
//    Thread.currentThread().setName(getClass().getSimpleName());
//    switch (state) {
//      case SubscriptionState.SUBSCRIBING -> log.info("Subscribing for orders updates");
//      case SubscriptionState.SUBSCRIBED -> log.info("Subscribed for orders updates");
//      case SubscriptionState.UNSUBSCRIBING -> log.info("Unsubscribing from orders updates");
//      case SubscriptionState.UNSUBSCRIBED -> log.info("Unsubscribed from orders updates");
//    }
//  }
//
//  @Override
//  public ConsumerState getState() {
//    return ConsumerState.ACTIVE;
//  }
//}
