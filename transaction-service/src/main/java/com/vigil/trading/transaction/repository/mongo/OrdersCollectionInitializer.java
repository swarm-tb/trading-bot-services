package com.vigil.trading.transaction.repository.mongo;

import jakarta.annotation.PostConstruct;
import lombok.val;
import org.bson.Document;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.CollectionOptions.TimeSeriesOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.CompoundIndexDefinition;
import org.springframework.stereotype.Component;

import static com.vigil.trading.integration.models.order.Constant.META_FIELD;
import static com.vigil.trading.integration.models.order.Constant.ORDERS;
import static com.vigil.trading.integration.models.order.Constant.SORT_ASC;
import static com.vigil.trading.integration.models.order.Constant.TIMESTAMP_FIELD;

@Component
public class OrdersCollectionInitializer {

  private final MongoTemplate mongoTemplate;

  public OrdersCollectionInitializer(final MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @PostConstruct
  public void initStrategyRecordsCollection() {
    val options = TimeSeriesOptions.timeSeries(TIMESTAMP_FIELD).metaField(META_FIELD);
    if (!mongoTemplate.collectionExists(ORDERS)) {
      val collectionOptions = CollectionOptions.empty().timeSeries(options);
      mongoTemplate.createCollection(ORDERS, collectionOptions);
      val indexDefinition = new CompoundIndexDefinition(
          new Document(TIMESTAMP_FIELD, SORT_ASC).append(META_FIELD, SORT_ASC));
      mongoTemplate.indexOps(ORDERS).ensureIndex(indexDefinition);
    }
  }
}
