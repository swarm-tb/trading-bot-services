package com.vigil.trading.transaction.order

import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.metaapi.MetaApiOrder
import com.vigil.trading.integration.metaapi.MetaApiOrderServiceGrpc
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.SbAccountFacade
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

abstract class AbstractMetaApiOrderService protected constructor(
    private val orderServiceBlockingStub: MetaApiOrderServiceGrpc.MetaApiOrderServiceBlockingStub,
    private val accountFacade: SbAccountFacade,
    transactionProperties: TransactionProperties
) : AbstractOrderService(transactionProperties) {

    protected abstract val accountId: String?

    override fun makeMarketBuyOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        val symbol = tradingActionInfo.symbolInfo.symbol()
        val stopLossPrice = tradingActionInfo.price!! * stopLossPriceCoefficient
        logOrder(OrderSide.BUY, symbol, quantity, stopLossPrice)
        val orderResponse = orderServiceBlockingStub.createMarketBuyOrder(
            getRequest(
                symbol, quantity, stopLossPrice
            )
        )
        log.info("Order ID: {}", orderResponse.orderId)
        sleep()
        accountFacade.updatePositions(account)
    }

    override fun makeMarketSellOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        val symbol = tradingActionInfo.symbolInfo.symbol()
        val stopLossPrice = tradingActionInfo.price!! / stopLossPriceCoefficient
        logOrder(OrderSide.SELL, symbol, quantity, stopLossPrice)
        val orderResponse = orderServiceBlockingStub.createMarketSellOrder(
            getRequest(
                symbol, quantity, stopLossPrice
            )
        )
        log.info("Order ID: {}", orderResponse.orderId)
        sleep()
        accountFacade.updatePositions(account)
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        closePosition(tradingActionInfo.symbolInfo.symbol())
    }

    override fun closePosition(position: SbPosition) {
        closePosition(position.symbol)
    }

    private fun closePosition(symbol: String) {
        log.info(
            "Making order, Account: {}, Side: {}, Symbol: {}", account, SbPositionSide.CLOSED, symbol
        )
        val orderResponse = orderServiceBlockingStub.closePosition(
            MetaApiOrder.MetaApiClosePositionRequest.newBuilder().setAccountId(accountId).setSymbol(symbol).build()
        )
        log.info("Order ID: {}", orderResponse.orderId)
        sleep()
        accountFacade.updatePositions(account)
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo, position: SbPosition) {
        closePosition(tradingActionInfo)
    }

    private fun getRequest(
        symbol: String, size: BigDecimal, stopLossPrice: BigDecimal
    ) = MetaApiOrder.MetaApiOrderRequest.newBuilder().setAccountId(accountId).setSymbol(symbol)
        .setVolume(size.toDouble()).setStopLossPrice(stopLossPrice.toDouble()).build()

    companion object {
        private const val TIME_TO_SLEEP = 500

        //Sleep is needed when using terminalState to get positions
        private fun sleep() {
            try {
                TimeUnit.MILLISECONDS.sleep(TIME_TO_SLEEP.toLong())
            } catch (e: InterruptedException) {
                throw RuntimeException(e)
            }
        }
    }
}
