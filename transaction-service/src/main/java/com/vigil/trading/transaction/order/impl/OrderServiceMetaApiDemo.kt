package com.vigil.trading.transaction.order.impl

import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.metaapi.MetaApiOrderServiceGrpc
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.order.AbstractMetaApiOrderService
import org.springframework.stereotype.Service

@Service
class OrderServiceMetaApiDemo(
    orderServiceBlockingStub: MetaApiOrderServiceGrpc.MetaApiOrderServiceBlockingStub,
    accountFacade: SbAccountFacade,
    commonProperties: CommonProperties,
    transactionProperties: TransactionProperties
) : AbstractMetaApiOrderService(orderServiceBlockingStub, accountFacade, transactionProperties) {
    override val account = SbAccount.META_API_DEMO
    override val accountId = commonProperties.getProps(account)?.accountId
}
