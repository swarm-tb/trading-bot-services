package com.vigil.trading.transaction.consumer

import com.vigil.trading.integration.clients.StateClient
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.TradingAction
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.transaction.SbTransactionFacade
import com.vigil.trading.transaction.TransactionProperties
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class TradingActionConsumer(
    private val stateClient: StateClient,
    private val transactionFacade: SbTransactionFacade,
    private val transactionProperties: TransactionProperties
) {

    @KafkaListener(
        topics = ["#{commonProperties.getKafka().getTradingActionsTopic()}"],
        groupId = "#{commonProperties.getKafka().getTransactionServiceGroupId()}",
        id = "\${service-name.transaction-service}"
    )
    fun consumeTradingActions(tradingActionInfo: SbTradingActionInfo) {
        val account = tradingActionInfo.symbolInfo.account()
        if (!transactionProperties.getAccountProps(account).transactionsEnabled) {
            log.debug("Transactions are disabled for {}", account)
            return
        }
        log.trace("Processing record: {}", tradingActionInfo)
        processTradingAction(tradingActionInfo)
    }

    private fun processTradingAction(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val account = symbolInfo.account()
        val symbol = symbolInfo.symbol()
        try {
            when (tradingActionInfo.tradingAction) {
                TradingAction.SELL -> transactionFacade.shortPosition(tradingActionInfo)
                TradingAction.BUY -> transactionFacade.longPosition(tradingActionInfo)
                TradingAction.CLOSE -> transactionFacade.closePosition(tradingActionInfo)
                else -> log.trace("Account: {}, Neutral action for: {}", account, symbol)
            }
            if (account != SbAccount.SIMULATION) {
                //TODO: Send event instead of calling directly
                stateClient.refreshLastChangeTimestamp(account, symbol)
            }
        } catch (e: Exception) {
            log.error(
                "Account: {}, Symbol: {}, Exception during message processing: {}",
                account,
                symbol,
                e.message
            )
            log.debug("Stack trace:", e)
        }
    }

    companion object {
        private val log by logger()
    }
}
