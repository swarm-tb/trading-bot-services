package com.vigil.trading.transaction.order

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.order.impl.*
import org.springframework.stereotype.Component
import java.math.BigDecimal

@Component
class SbOrderFacade(
    ordersServiceSimulation: OrdersServiceSimulation,
    orderServiceBinance: OrderServiceBinance,
    orderServiceMetaApiDemo: OrderServiceMetaApiDemo,
    orderServiceMetaApiFTMO: OrderServiceMetaApiFTMO,
    orderServiceMetaApiFidelcrest: OrderServiceMetaApiFidelcrest,
    orderServiceMetaApiSurgeTrader: OrderServiceMetaApiSurgeTrader
) {
    private val orderServiceMap: Map<SbAccount, SbOrderService> = mapOf(
        SbAccount.SIMULATION to ordersServiceSimulation,
        SbAccount.META_API_FTMO to orderServiceMetaApiFTMO,
        SbAccount.BINANCE_USD_FUTURES to orderServiceBinance,
        SbAccount.META_API_DEMO to orderServiceMetaApiDemo,
        SbAccount.META_API_FIDELCREST to orderServiceMetaApiFidelcrest,
        SbAccount.META_API_SURGE_TRADER to orderServiceMetaApiSurgeTrader
    )

    fun makeMarketSellOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        orderServiceMap[tradingActionInfo.symbolInfo.account()]!!.makeMarketSellOrder(tradingActionInfo, quantity)
    }

    fun makeMarketBuyOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        orderServiceMap[tradingActionInfo.symbolInfo.account()]!!.makeMarketBuyOrder(tradingActionInfo, quantity)
    }

    fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        orderServiceMap[tradingActionInfo.symbolInfo.account()]!!.closePosition(tradingActionInfo)
    }

    fun closePosition(tradingActionInfo: SbTradingActionInfo, position: SbPosition) {
        orderServiceMap[tradingActionInfo.symbolInfo.account()]!!.closePosition(tradingActionInfo, position)
    }

    fun closePosition(account: SbAccount, position: SbPosition) {
        orderServiceMap[account]!!.closePosition(position)
    }
}
