package com.vigil.trading.transaction.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class RoundingUtils {

  private RoundingUtils() {
  }

  public static BigDecimal getRoundedVolume(
      final BigDecimal quantity, final BigDecimal stepSize
  ) {
    final int scale = stepSize.stripTrailingZeros().scale();
    return quantity.setScale(scale, RoundingMode.DOWN);
  }
}
