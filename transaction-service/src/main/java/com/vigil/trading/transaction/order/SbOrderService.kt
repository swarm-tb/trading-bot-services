package com.vigil.trading.transaction.order

import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import java.math.BigDecimal

interface SbOrderService {
    fun makeMarketSellOrder(tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal)
    fun makeMarketBuyOrder(tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal)
    fun closePosition(tradingActionInfo: SbTradingActionInfo)
    fun closePosition(tradingActionInfo: SbTradingActionInfo, position: SbPosition)
    fun closePosition(position: SbPosition)
}
