package com.vigil.trading.transaction.account.container

import com.vigil.trading.integration.records.SbPosition
import java.math.BigDecimal

class PositionsTotalCostContainer {
    private val symbolPositions: MutableMap<String, SbPosition> = HashMap()

    fun getPositionDelta(position: SbPosition, price: BigDecimal): BigDecimal {
        val symbol: String = position.symbol
        val previousPosition = symbolPositions[symbol]
        if (previousPosition == null) {
            symbolPositions[symbol] = position
            return BigDecimal.ZERO
        }
        //TODO make cost required or remove it
        val currentCost = position.cost!!
        val costDelta = currentCost - previousPosition.cost!!
        symbolPositions[symbol] = SbPosition(
            symbol, position.positionSide, position.volume, currentCost, price, position.stopLossPrice
        )
        val sizeDelta = position.volume - previousPosition.volume
        return costDelta - sizeDelta * price
    }

    fun clear() {
        symbolPositions.clear()
    }
}
