package com.vigil.trading.transaction

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.transaction.constant.SbAccountType
import org.springframework.boot.context.properties.ConfigurationProperties
import java.math.BigDecimal
import java.time.Duration

@ConfigurationProperties(prefix = "transaction")
data class TransactionProperties(
    var simulationId: String,
    var accountUpdateInterval: Duration,
    var accountProps: Map<SbAccount, AccountProps>,
    var commissionPercent: BigDecimal,

) {
    fun getAccountProps(account: SbAccount): AccountProps = accountProps[account]!!
}

data class AccountProps(
    var leverage: BigDecimal,
    var transactionsEnabled: Boolean,
    var maxDailyLossPercent: BigDecimal,
    var positionStopLossFraction: BigDecimal?,
    var initialBalance: BigDecimal,
    var profitTargetPercent: BigDecimal,
    var accountType: SbAccountType
)
