package com.vigil.trading.transaction.constant

enum class SbAccountType {
    NETTING,
    HEDGING
}
