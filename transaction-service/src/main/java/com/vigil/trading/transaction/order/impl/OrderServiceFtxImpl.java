// package com.vigil.trading.transaction.impl;
//
// import com.vigil.trading.integration.records.StrategyRecord;
// import com.vigil.trading.transaction.order.SbOrderService;
// import io.contek.invoker.ftx.api.rest.user.PostOrders;
// import io.contek.invoker.ftx.api.rest.user.UserRestApi;
// import lombok.extern.slf4j.Slf4j;
// import org.springframework.context.annotation.Profile;
// import org.springframework.stereotype.Service;
// import org.ta4j.core.num.Num;
//
// import static com.vigil.trading.integration.constant.Profiles.BINANCE;
//
// @Slf4j
// @Service
// @Profile(BINANCE)
// public class OrderServiceFtxImpl implements SbOrderService {
//  private static final String MARKET_TYPE = "market";
//  private static final String LIMIT_TYPE = "limit";
//  private static final String BUY_SIDE = "buy";
//  private static final String SELL_SIDE = "sell";
//
//  private final UserRestApi userRestApi;
//  private final TradingPairUtils tradingPairUtils;
//
//  public OrderServiceFtxImpl(
//      final UserRestApi userRestApi, final TradingPairUtils tradingPairUtils) {
//    this.userRestApi = userRestApi;
//    this.tradingPairUtils = tradingPairUtils;
//  }
//
//  @Override
//  public void makeMarketBuyOrder(final StrategyRecord strategyRecord, final Num quantity) {
//    makeMarketOrder(strategyRecord, BUY_SIDE, quantity);
//  }
//
//  @Override
//  public void makeMarketSellOrder(final StrategyRecord strategyRecord, final Num quantity) {
//    makeMarketOrder(strategyRecord, SELL_SIDE, quantity);
//  }
//
//  @Override
//  public void makeLimitBuyOrder(final String symbol, final Num quantity, final Num price) {
//    makeLimitOrder(symbol, BUY_SIDE, quantity, price);
//  }
//
//  @Override
//  public void makeLimitSellOrder(final String symbol, final Num quantity, final Num price) {
//    makeLimitOrder(symbol, SELL_SIDE, quantity, price);
//  }
//
//  private void makeMarketOrder(
//      final StrategyRecord strategyRecord, final String side, final Num quantity) {
//    final String symbol = strategyRecord.SbSymbolInfo().symbol();
//    log.info("Side: {}, Symbol: {}, Amount: {}", side, symbol, quantity);
//    final PostOrders.Response response =
//        userRestApi
//            .postOrders()
//            .setSide(side)
//            .setType(MARKET_TYPE)
//            .setMarket(symbol)
//            .setSize(quantity.doubleValue())
//            .submit();
//    log.debug("Order id: {}", response.result.id);
//  }
//
//  private void makeLimitOrder(
//      final String symbol, final String side, final Num quantity, final Num price) {
//    log.info("Side: {}, Symbol: {}, Amount: {}, Price: {}", side, symbol, quantity, price);
//    final PostOrders.Response response =
//        userRestApi
//            .postOrders()
//            .setSide(side)
//            .setType(LIMIT_TYPE)
//            .setPostOnly(true)
//            .setMarket(symbol)
//            .setPrice(price.doubleValue())
//            .setSize(quantity.doubleValue())
//            .submit();
//    log.debug("Order id: {}", response.result.id);
//  }
// }
