package com.vigil.trading.transaction.account.stat

import com.vigil.trading.integration.utility.logger
import org.apache.commons.math3.stat.descriptive.rank.Percentile
import java.math.BigDecimal
import java.math.BigDecimal.valueOf

class MaxDrawdownHistory {

    companion object {
        private val log by logger()
    }

    private val maxDailyPercentageArray: MutableList<Double> = mutableListOf()

    fun addMaxDailyPercentage(maxDailyPercentage: Double) {
        maxDailyPercentageArray.add(maxDailyPercentage)
    }

    fun get80Percentile(): BigDecimal {
        if (maxDailyPercentageArray.isEmpty()) {
            log.warn("Max daily percentage array is empty")
            return valueOf(0)
        }
        val percentile = Percentile()
        percentile.data = maxDailyPercentageArray.toDoubleArray()
        log.info("Percentile dataset size: {}", maxDailyPercentageArray.size)
        return valueOf(percentile.evaluate(80.0))
    }
}
