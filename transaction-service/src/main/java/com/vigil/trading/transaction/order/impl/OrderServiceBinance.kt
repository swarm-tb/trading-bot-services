package com.vigil.trading.transaction.order.impl

import com.vigil.trading.binance.BinanceOrderService
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.order.AbstractOrderService
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class OrderServiceBinance(
    private val binanceOrderService: BinanceOrderService,
    private val accountFacade: SbAccountFacade,
    transactionProperties: TransactionProperties
) : AbstractOrderService(transactionProperties) {

    override val account = SbAccount.BINANCE_USD_FUTURES

    override fun makeMarketBuyOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        val symbol = tradingActionInfo.symbolInfo.symbol()
        logOrder(OrderSide.BUY, symbol, quantity, BigDecimal.ZERO)
        binanceOrderService.makeMarketBuyOrder(symbol, quantity)
        accountFacade.updatePositions(account)
    }

    override fun makeMarketSellOrder(
        tradingActionInfo: SbTradingActionInfo, quantity: BigDecimal
    ) {
        val symbol = tradingActionInfo.symbolInfo.symbol()
        logOrder(OrderSide.SELL, symbol, quantity, BigDecimal.ZERO)
        binanceOrderService.makeMarketSellOrder(symbol, quantity)
        accountFacade.updatePositions(account)
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        throw UnsupportedOperationException()
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo, position: SbPosition) {
        closePosition(tradingActionInfo)
    }

    override fun closePosition(position: SbPosition) {
        val quantity = position.volume.abs()
        when (position.positionSide) {
            SbPositionSide.LONG -> binanceOrderService.makeMarketSellOrder(position.symbol, quantity)
            SbPositionSide.SHORT -> binanceOrderService.makeMarketBuyOrder(position.symbol, quantity)
            else -> log.error(
                "Account: {}, Symbol: {}, Wrong order side: {}", account, position.symbol, position.positionSide
            )
        }
    }
}
