package com.vigil.trading.transaction.impl

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.constant.SbPositionSide
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.half
import com.vigil.trading.integration.utility.isZero
import com.vigil.trading.transaction.AbstractTransactionService
import com.vigil.trading.transaction.SbBalancingService
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.order.SbOrderFacade
import org.springframework.stereotype.Service

@Service
class TransactionServiceNetting(
    orderFacade: SbOrderFacade, accountFacade: SbAccountFacade, balancingService: SbBalancingService
) : AbstractTransactionService(orderFacade, accountFacade, balancingService) {

    override fun longPosition(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val position = accountFacade.getPosition(symbolInfo.account(), symbolInfo.symbol())
        if (position == null) {
            orderFacade.makeMarketBuyOrder(tradingActionInfo, getExpectedPositionVolume(tradingActionInfo))
            return
        }
        val expectedPositionVolume = getExpectedPositionVolume(tradingActionInfo)
        val volumeDelta = expectedPositionVolume - position.volume
        if (volumeDelta > expectedPositionVolume.half()) {
            orderFacade.makeMarketBuyOrder(tradingActionInfo, volumeDelta)
        }
    }

    override fun shortPosition(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val position = accountFacade.getPosition(symbolInfo.account(), symbolInfo.symbol())
        if (position == null) {
            orderFacade.makeMarketSellOrder(tradingActionInfo, getExpectedPositionVolume(tradingActionInfo))
            return
        }
        val expectedPositionVolume = getExpectedPositionVolume(tradingActionInfo)
        val volumeDelta = expectedPositionVolume + position.volume
        if (volumeDelta >= expectedPositionVolume) {
            orderFacade.makeMarketSellOrder(tradingActionInfo, volumeDelta)
        } else if (volumeDelta < expectedPositionVolume.negate()) {
            orderFacade.makeMarketBuyOrder(tradingActionInfo, volumeDelta.abs())
        }
    }

    override fun closePosition(tradingActionInfo: SbTradingActionInfo) {
        val symbolInfo = tradingActionInfo.symbolInfo
        val account = symbolInfo.account()
        val symbol = symbolInfo.symbol()
        val position = accountFacade.getPosition(account, symbol)
        if (position == null) {
            log.trace("Account: {}, No position to close for {}", account, symbol)
            return
        }
        when (position.positionSide) {
            SbPositionSide.LONG -> orderFacade.makeMarketSellOrder(tradingActionInfo, position.volume)
            SbPositionSide.SHORT -> orderFacade.makeMarketBuyOrder(tradingActionInfo, position.volume.abs())
            else -> log.error(
                "Account: {}, Symbol: {}, Wrong order side: {}", account, symbol, position.positionSide
            )
        }
    }

    /**
     * Close all positions with the specified position side. Can't be used for simulation account, refactoring needed.
     */
    override fun closePositions(account: SbAccount, positionSide: SbPositionSide?) {
        val positionsCache = accountFacade.getPositionsCache(account)
        if (positionSide == null || positionSide == SbPositionSide.CLOSED) {
            log.error("Account: {}, Wrong position side: {}, positions won't be closed", account, positionSide)
            return
        }
        positionsCache.filter { !it.value.volume.isZero() }.forEach { (_, position) ->
            if (position.positionSide == positionSide) {
                closePosition(account, position)
            }
        }
    }

    private fun closePosition(account: SbAccount, position: SbPosition) {
        try {
            orderFacade.closePosition(account, position)
        } catch (e: Exception) {
            log.error("Account: {}, Can't close position: {}, Error: {}", account, position, e.message)
            log.debug("Stack trace: ", e)
        }
    }
}
