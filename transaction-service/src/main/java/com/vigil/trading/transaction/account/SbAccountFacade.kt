package com.vigil.trading.transaction.account

import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.dto.history.AccountBalanceHistory
import com.vigil.trading.integration.records.SbAccountInfo
import com.vigil.trading.integration.records.SbPosition
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.account.impl.*
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.time.Instant

@Component
class SbAccountFacade(
    accountServiceBinance: AccountServiceBinance,
    accountServiceMetaApiDemo: AccountServiceMetaApiDemo,
    accountServiceMetaApiFTMO: AccountServiceMetaApiFTMO,
    accountServiceMetaApiFidelcrest: AccountServiceMetaApiFidelcrest,
    accountServiceMetaApiSurgeTrader: AccountServiceMetaApiSurgeTrader,
    private val accountServiceSimulation: SbAccountServiceSimulation
) {
    private val serviceMap: Map<SbAccount, SbAccountService> = mapOf(
        SbAccount.SIMULATION to accountServiceSimulation,
        SbAccount.META_API_DEMO to accountServiceMetaApiDemo,
        SbAccount.BINANCE_USD_FUTURES to accountServiceBinance,
        SbAccount.META_API_FTMO to accountServiceMetaApiFTMO,
        SbAccount.META_API_FIDELCREST to accountServiceMetaApiFidelcrest,
        SbAccount.META_API_SURGE_TRADER to accountServiceMetaApiSurgeTrader
    )

    fun updatePositions(account: SbAccount) {
        serviceMap[account]!!.updatePositions()
    }

    fun getAccountInfo(account: SbAccount, timestamp: Instant): SbAccountInfo {
        return serviceMap[account]!!.getAccountInfo(timestamp)
    }

    fun getPosition(account: SbAccount, symbol: String): SbPosition? {
        return serviceMap[account]!!.getPosition(symbol)
    }

    fun updatePositions(position: SbPosition, commission: BigDecimal) {
        accountServiceSimulation.updatePosition(position, commission)
    }

    fun getPositionsCache(account: SbAccount): Map<String, SbPosition> {
        return serviceMap[account]!!.positionsCache
    }

    fun getAccountBalanceHistory(): AccountBalanceHistory {
        val maxDrawdown = accountServiceSimulation.maxDrawdown
        return AccountBalanceHistory(
            accountServiceSimulation.equity,
            maxDrawdown.maxTrailingPercentage,
            maxDrawdown.maxTrailingTimestamp,
            maxDrawdown.overallMaxDailyPercentage,
            maxDrawdown.maxDailyTimestamp,
            maxDrawdown.maxDrawdownHistory.get80Percentile(),
            accountServiceSimulation.equityHistory
        )
    }

    fun isAccountBalanceLimitReached(tradingActionInfo: SbTradingActionInfo): Boolean {
        val accountService = serviceMap[tradingActionInfo.symbolInfo.account()]
        return accountService!!.isAccountBalanceLimitReached(tradingActionInfo.timestamp!!)
    }
}
