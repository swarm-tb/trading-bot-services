package com.vigil.trading.transaction

import com.vigil.trading.integration.records.SbSymbolInfo
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.integration.utility.logger
import com.vigil.trading.transaction.account.SbAccountFacade
import com.vigil.trading.transaction.exception.TransactionException
import com.vigil.trading.transaction.order.SbOrderFacade
import com.vigil.trading.transaction.utility.RoundingUtils
import java.math.BigDecimal

abstract class AbstractTransactionService protected constructor(
    protected val orderFacade: SbOrderFacade,
    protected val accountFacade: SbAccountFacade,
    private val balancingService: SbBalancingService
) : SbTransactionService {

    protected val log by logger()

    protected fun getExpectedPositionVolume(
        tradingActionInfo: SbTradingActionInfo
    ): BigDecimal {
        val symbolInfo = tradingActionInfo.symbolInfo
        val positionCost = balancingService.getPositionCost(
            symbolInfo.account, tradingActionInfo.timestamp
        )
        val expectedVolume = positionCost / tradingActionInfo.price!! / symbolInfo.contractSize
        return prepareVolume(symbolInfo, expectedVolume)
    }

    protected fun isVolumeDeltaGreaterThanThreshold(
        expectedPositionVolume: BigDecimal,
        currentPositionVolume: BigDecimal,
        deltaFunction: (BigDecimal, BigDecimal) -> BigDecimal
    ): Boolean {
        val volumeDelta = deltaFunction(expectedPositionVolume, currentPositionVolume)
        return volumeDelta > expectedPositionVolume * EXPECTED_POSITION_FRACTION
    }

    private fun prepareVolume(symbolInfo: SbSymbolInfo, quantity: BigDecimal): BigDecimal {
        val volume = getValidatedVolume(symbolInfo, quantity)
        val stepSize = symbolInfo.sizeIncrement()
        log.debug("Quantity: {}, Step size: {}", volume, stepSize)
        return RoundingUtils.getRoundedVolume(volume, stepSize)
    }

    private fun getValidatedVolume(symbolInfo: SbSymbolInfo, volume: BigDecimal): BigDecimal {
        if (volume < symbolInfo.minSize()) {
            throw TransactionException.MinSizeException(
                symbolInfo.account, symbolInfo.symbol(), volume, symbolInfo.minSize()
            )
        }
        val maxVolume = symbolInfo.maxSize()
        return if (volume > maxVolume) logAndReturnMax(symbolInfo, volume, maxVolume) else volume
    }

    private fun logAndReturnMax(symbolInfo: SbSymbolInfo, volume: BigDecimal, maxVolume: BigDecimal): BigDecimal {
        log.warn(
            "Account: {}, Symbol: {}, Expected position volume {} is greater than max allowed volume {}. " +
                    "Max allowed volume will be used.",
            symbolInfo.account,
            symbolInfo.symbol,
            volume,
            maxVolume
        )
        return maxVolume
    }

    companion object {
        private val EXPECTED_POSITION_FRACTION = BigDecimal.valueOf(0.8)
    }
}
