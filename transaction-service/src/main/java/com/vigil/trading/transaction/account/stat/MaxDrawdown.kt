package com.vigil.trading.transaction.account.stat

import com.vigil.trading.integration.utility.getPercentageOf
import java.math.BigDecimal
import java.time.Instant
import java.time.temporal.ChronoUnit

class MaxDrawdown {
    var maxTrailingPercentage: BigDecimal = BigDecimal.ZERO
    var maxTrailingTimestamp: Instant? = null
    var overallMaxDailyPercentage: BigDecimal = BigDecimal.ZERO
    var lastMaxDailyPercentage: BigDecimal = BigDecimal.ZERO
    var maxDailyTimestamp: Instant? = null
    var maxDrawdownHistory: MaxDrawdownHistory = MaxDrawdownHistory()

    private var maxPeak: BigDecimal = BigDecimal.ZERO
    private var maxDailyPeak: BigDecimal = BigDecimal.ZERO
    private var currentDay: Instant? = null

    fun processTrailing(equity: BigDecimal, timestamp: Instant) {
        maxPeak = equity.max(maxPeak)
        val drawdown = maxPeak - equity
        val drawdownPercentage = drawdown.getPercentageOf(maxPeak)
        if (drawdownPercentage > maxTrailingPercentage) {
            maxTrailingPercentage = drawdownPercentage
            maxTrailingTimestamp = timestamp
        }
    }

    fun processDaily(equity: BigDecimal, timestamp: Instant) {
        refreshValuesIfDayChanges(timestamp.truncatedTo(ChronoUnit.DAYS))
        maxDailyPeak = equity.max(maxDailyPeak)
        val drawdown = maxDailyPeak.subtract(equity)
        val drawdownPercentage = drawdown.getPercentageOf(maxDailyPeak)
        evaluateMaxDailyPercentage(timestamp, drawdownPercentage)
    }

    private fun refreshValuesIfDayChanges(day: Instant) {
        if (currentDay == null) {
            currentDay = day
        } else if (currentDay != day) {
            if (lastMaxDailyPercentage > BigDecimal.ZERO) {
                maxDrawdownHistory.addMaxDailyPercentage(lastMaxDailyPercentage.toDouble())
                lastMaxDailyPercentage = BigDecimal.ZERO
            }
            currentDay = day
            maxDailyPeak = BigDecimal.ZERO
        }
    }

    private fun evaluateMaxDailyPercentage(timestamp: Instant, drawdownPercentage: BigDecimal) {
        if (drawdownPercentage > lastMaxDailyPercentage) {
            lastMaxDailyPercentage = drawdownPercentage
        }
        if (drawdownPercentage > overallMaxDailyPercentage) {
            overallMaxDailyPercentage = drawdownPercentage
            maxDailyTimestamp = timestamp
        }
    }
}
