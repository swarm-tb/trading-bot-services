package com.vigil.trading.transaction.account.impl

import com.vigil.trading.integration.CommonProperties
import com.vigil.trading.integration.clients.SymbolClient
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.metaapi.MetaApiAccountServiceGrpc
import com.vigil.trading.integration.records.SbTradingActionInfo
import com.vigil.trading.transaction.TransactionProperties
import com.vigil.trading.transaction.account.AbstractMetaApiAccountService
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class AccountServiceMetaApiFidelcrest(
    accountServiceBlockingStub: MetaApiAccountServiceGrpc.MetaApiAccountServiceBlockingStub,
    commonProperties: CommonProperties,
    symbolClient: SymbolClient,
    transactionProperties: TransactionProperties,
    tradingActionKafkaTemplate: KafkaTemplate<String, SbTradingActionInfo>
) : AbstractMetaApiAccountService(
    accountServiceBlockingStub,
    symbolClient,
    transactionProperties,
    commonProperties,
    tradingActionKafkaTemplate
) {
    override val account = SbAccount.META_API_FIDELCREST
    override val accountId = commonProperties.getProps(account)?.accountId
}
