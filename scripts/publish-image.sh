#!/bin/bash

PROJECT_NAME="$1"
PROJECT_PATH="$CI_REGISTRY_IMAGE/$PROJECT_NAME"
CACHE_PATH="$PROJECT_PATH/paketo-build-cache"
VERSION=$(grep '^version' "$PROJECT_NAME/build.gradle.kts" | awk '{print $3}' | tr -d "\"")

/cnb/lifecycle/creator -app "$PROJECT_NAME" -cache-image "$CACHE_PATH:$VERSION" "$PROJECT_PATH:$VERSION"
